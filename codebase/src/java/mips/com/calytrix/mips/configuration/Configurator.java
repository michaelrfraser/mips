/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.logging.log4j.Logger;

import com.calytrix.mips.Messages;
import com.calytrix.mips.MipsException;

/**
 * To make things a bit cleaner, this class is responsible for the actual process of turning
 * a command line into a populated {@link Configuration} object.
 * 
 * The general process is:
 * 
 *   1. Create a blank `Configuration` object to populate
 *   2. Load the command line arguments and log them
 *   3. Pull out any important command line args that will affect config file loading
 *   4. Load the config file into a `Properties` and push that into our blank `Configuration`
 *   5. Override any values in the `Configuration` with any provided on the command line
 */
public class Configurator
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	private Configurator()
	{}

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	public Configuration loadConfiguration( String... commandline )
	{
		//////////////////////////////////////////
		// load the default configuration   //////
		//////////////////////////////////////////
		Configuration configuration = new Configuration();

		///////////////////////////////////////////////
		// load from the command line arguments   /////
		///////////////////////////////////////////////
		Map<Argument,String> overrides = loadCommandLine( commandline );
		preload( configuration, overrides ); // pull out anything we may need to parse the config

		///////////////////////////////////////////
		// load from the configuration file   /////
		///////////////////////////////////////////
		File configurationFile = configuration.getConfigurationFile();
		if( configurationFile.exists() )
		{
			// configuration file exists, load the properties into it
			Properties properties = new Properties();
			try
			{
				properties.load( configurationFile.toURI().toURL().openStream() );
			}
			catch( Exception e )
			{
				String message = String.format( Messages.get("Configurator.error.parse"), 
				                                e.getLocalizedMessage() );
				throw new ConfigurationException( message, e );
			}

			// store the loaded configuration 
			configuration.override( properties );
		}

		////////////////////////////////////////////////////
		// override config with command line options   /////
		////////////////////////////////////////////////////
		for( Argument argument : overrides.keySet() )
		{
			// only take the override if the argument declares a config property and a non-null,
			// non-empty value has been provided
			if( argument.getConfigProperty() == null )
				continue;
			
			String value = overrides.getOrDefault( argument, argument.getDefaultValue() );
			if( value == null || value.trim().equals("") )
				continue;
			
			configuration.override( argument.getConfigProperty(), value );
		}

		/////////////////////////////////////////////////////////////////////////////////////
		// Post Load                                                                       //
		//                                                                                 //
		// Reparse any things for the logging. Because this is contained off in its own    //
		// class, we can't just determine the values for the contained properties when     //
		// when someone wants to know. The default configuration will just apply and never //
		// be updated, so we have to actively look for log file things and push them out   //
		// of the config file and into the log4j configuration object                      //
		/////////////////////////////////////////////////////////////////////////////////////
		postload( configuration );

		
		////////////////////////////////////////////////////////////
		// initialize the logging and tell it what args we loaded //
		////////////////////////////////////////////////////////////
		configuration.getLog4jConfiguration().activateConfiguration();
		Logger logger = configuration.getMipsLogger();
		logger.info( Messages.get("Configurator.welcome") );
		logger.info( "" );

		// log the command line arguments
		if( overrides.isEmpty() == false )
		{
			int maxlength = 0;
			for( Argument argument : overrides.keySet() )
			{
				if( argument.getArgumentName().length() > maxlength )
					maxlength = argument.getArgumentName().length();
			}

			logger.info( Messages.get("Configurator.overrides") );
			String formatString = "     %" + maxlength + "s = %s";
			for( Argument argument : overrides.keySet() )
			{
				logger.info( String.format(formatString,
				                           argument.getArgumentName(),
				                           overrides.get(argument)) );
			}
			logger.info( "" );
		}

		if( configurationFile.exists() == false )
		{
			logger.info( Messages.get("Configurator.filenotfound"), 
			             configurationFile.getAbsolutePath() );
			logger.info( Messages.get("Configurator.usingdefaults") );
		}

		/////////////////////////////////////////////////////////////
		// log all the configuration properties for debugging   /////
		/////////////////////////////////////////////////////////////
		if( logger.isDebugEnabled() )
			logAllProperties( logger, configuration, overrides );
		
		// Done - return the populated configuration
		return configuration;
	}

	/**
	 * Loads properties from the command line and puts them into the given configuration object.
	 * Returns a map of all the various command line arguments that were supplied and the values
	 * they were supplied with.
	 */
	private Map<Argument,String> loadCommandLine( String... commandline )
	{
		Map<Argument,String> loaded = new HashMap<Argument,String>();
		for( int i = 0; i < commandline.length; i++ )
		{
			Argument argument = Argument.getArgument( commandline[i] );
			if( argument.requiresParam() )
				loaded.put( argument, commandline[++i] );
			else
				loaded.put( argument, argument.getDefaultValue() );
		}
		
		return loaded;
	}

	/**
	 * Pull out anything we need to load the config before we actually try to start loading
	 */
	private void preload( Configuration configuration, Map<Argument,String> overrides )
	{
		// Internal defaults
		Properties internalDefaults = new Properties();
		try
		{
			InputStream defaultsStream = 
				this.getClass().getClassLoader().getResourceAsStream( "com/calytrix/mips/mips.properties" );
			internalDefaults.load( defaultsStream );
			configuration.override( internalDefaults );
		}
		catch( IOException ioe )
		{
			throw new MipsException( Messages.get("Configurator.error.nodefaults") );
		}
		
		// System defaults (sourced from ${mips.appdir}/etc")
		Properties systemDefaults = new Properties();
		try
		{
			File appdir = configuration.getMipsApplicationDir();
			File systemDefaultsFile = new File( appdir, "etc/mips.config" );
			if( systemDefaultsFile.exists() )
			{
				InputStream defaultsStream = new FileInputStream( systemDefaultsFile );
				systemDefaults.load( defaultsStream );
				configuration.override( systemDefaults );
			}
		}
		catch( IOException ioe )
		{
			// 
		}
		
		if( overrides.containsKey(Argument.MipsProfile) )
			configuration.setMipsProfileDir( new File(overrides.get(Argument.MipsProfile)) );
		
		if( overrides.containsKey(Argument.MipsOutput) )
			configuration.setMipsOutputDir( new File(overrides.get(Argument.MipsOutput)) );

		if( overrides.containsKey(Argument.ConfigFile) )
			configuration.setConfigurationFile( new File(overrides.get(Argument.ConfigFile)) );

		if( overrides.containsKey(Argument.LogLevel) )
			configuration.getLog4jConfiguration().setLevel( overrides.get(Argument.LogLevel) );

		if( overrides.containsKey(Argument.LogFile) )
			configuration.getLog4jConfiguration().setFile( overrides.get(Argument.LogFile) );
	}

	private void postload( Configuration configuration )
	{
		//
		// Check to see where we should put the log file
		//
		if( configuration.properties.contains(Configuration.KEY_LOG_FILE) )
		{
			// user has specified a location in the log file
			String logfile = configuration.getProperty( Configuration.KEY_LOG_FILE );
			logfile = new File(logfile).getAbsolutePath();
			configuration.getLog4jConfiguration().setFile( logfile );
		}
		else
		{
			// set default location
			File logfile = new File( configuration.getMipsProfileDir(), "mips.log" );
			configuration.getLog4jConfiguration().setFile( logfile );
		}

		//
		// Check what the log level should be
		//
		if( configuration.properties.containsKey(Configuration.KEY_LOG_LEVEL) )
		{
			String level = configuration.getProperty( Configuration.KEY_LOG_LEVEL );
			configuration.getLog4jConfiguration().setLevel( level );
		}
		
		//
		// Check whether we should log with a date
		//
		if( configuration.properties.containsKey(Configuration.KEY_LOG_DATE) )
		{
			String value = configuration.getProperty( Configuration.KEY_LOG_DATE, "false" );
			value = value.toLowerCase();
			if( value.equals("true") || value.equals("yes") || value.equals("on") )
				configuration.getLog4jConfiguration().setLogWithDate( true );
			else
				configuration.getLog4jConfiguration().setLogWithDate( false );
		}
	}
	
	/**
	 * Log all the configured properties from the config file and command line to the log file
	 */
	private void logAllProperties( Logger logger, Configuration configuration, Map<Argument,String> commandline )
	{
		// firstly, to make printing nice, figure out the longest property name
		List<String> keys = new ArrayList<String>(); // sort later and then use this to pull out in order
		int longestName = 0;
		for( Object key : configuration.properties.keySet() )
		{
			if( key.toString().length() > longestName )
				longestName = key.toString().length();
			
			keys.add( key.toString() );
		}
		
		logger.debug( Messages.get("Configurator.properties.hr") );
		logger.debug( Messages.get("Configurator.properties.title") );
		logger.debug( Messages.get("Configurator.properties.hr") );
		
		String formatString = " %"+longestName+"s = %s %s";
		Collections.sort( keys );
		for( String key : keys )
		{
			// was this specified on the command line? If so we'll include that info
			boolean isCommandLine = false;
			for( Argument argument : commandline.keySet() )
			{
				if( argument.getConfigProperty() != null &&
				    argument.getConfigProperty().equals(key.toString()) )
					isCommandLine = true;
			}
			
			String value = configuration.getProperty(key);
			String cline = isCommandLine ? "(" + Messages.get("Configurator.properties.commandline") + ")": "";
			logger.debug( String.format(formatString,key.toString(),value,cline) );
		}
		
		logger.debug( "" );
	}

	
	///////////////////////////////////////////////////////////////////////////////////////////
	/// SHUTDOWN HANDLING  ////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////
	// Methods that should be run on shutdown. We put this here because all the config
	// stuff is generally loaded from here anyway, so best to keep it all together
	public static void shutdown( Configuration configuration ) throws Exception
	{
		
	}
	
	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
	/**
	 * Using the given command line, generate a new {@link Configuration} object and return it.
	 * If there is a problem parsing any of the command line or configuration file, an exception
	 * will be thrown.
	 */
	public static Configuration load( String... arguments ) throws ConfigurationException
	{
		return new Configurator().loadConfiguration( arguments );
	}

}
