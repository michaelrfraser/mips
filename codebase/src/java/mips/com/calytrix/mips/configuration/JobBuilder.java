/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.configuration;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.calytrix.mips.Messages;
import com.calytrix.mips.MipsException;
import com.calytrix.mips.job.Job;
import com.calytrix.mips.task.ITask;
import com.calytrix.mips.task.TaskFactory;
import com.calytrix.mips.utils.JsonUtils;

/**
 * The JobBuilder class provides a convenience mechanism for creating and populating a {@link Job}
 * instances from a job file.
 */
public class JobBuilder 
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------
	
	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private TaskFactory taskFactory;
	private Properties baseProperties;
	private Logger logger;
	
	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	/**
	 * Constructor for the type JobBuilder
	 * 
	 * @param taskFactory the Task Factory that this builder will request task instances from
	 * @param baseProperties the base properties that will be inherited by all jobs built by this 
	 *                       JobBuilder
	 * @param logger the logger to log status/debug messages to
	 * 
	 * @see JobBuilder#makeBaseProperties(Configuration)
	 */
	public JobBuilder( TaskFactory taskFactory, 
	                   Properties baseProperties, 
	                   Logger logger )
	{
		this.taskFactory = taskFactory;
		this.logger = LogManager.getFormatterLogger( logger.getName() + ".jobbuilder" );
		this.baseProperties = baseProperties;
	}
	
	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	/**
	 * Parses the specified job file and builds a corresponding {@link Job} instance for each job 
	 * definition that the file contains
	 * 
	 * @param jobFile the file that contains the job descriptions
	 * @return a list of {@link Job} instances representing the jobs defined within the job file
	 */
	public List<Job> fromFile( File jobFile )
	{
		this.logger.info( Messages.get("JobBuilder.loadstart"),
		                  jobFile.getAbsolutePath() );
		
		// Preliminary checks, does the file exist? Is it actually a file?
		String absPath = jobFile.getAbsolutePath();
		if( !jobFile.exists() )
		{
			String message = String.format( Messages.get("JobBuilder.error.filenotfound"),
			                                absPath );
			throw new MipsException( message );
		}
		
		if( !jobFile.isFile() )
		{
			String message = String.format(Messages.get( "JobBuilder.error.pathnotfile"), 
			                               absPath );
			throw new MipsException( message );
		}
		
		// Attempt to parse the file to JSON
		JSONObject root = null;
		try
		{
			FileReader fileReader = new FileReader( jobFile );
			JSONParser parser = new JSONParser();
			root = (JSONObject)parser.parse( fileReader );
		}
		catch( Exception e )
		{
			String message = String.format( Messages.get("JobBuilder.error.jsonparse"),
			                                absPath,
			                                e.getLocalizedMessage() );
			throw new MipsException( message, e );
		}
		
		// Process the job definitions within the file
		JSONArray jobsNode = (JSONArray)root.get( "jobs" );
		List<Job> result = processJobsNode( jobsNode );
		
		logger.info( Messages.get("JobBuilder.loadsuccess"), 
		             jobFile.getAbsolutePath(),
		             result.size() );
		
		return result;
	}
	
	private List<Job> processJobsNode( JSONArray jobs )
	{
		List<Job> result = new ArrayList<>( jobs.size() );
		for( int i = 0 ; i < jobs.size() ; ++i )
		{
			JSONObject jobNode = (JSONObject)jobs.get( i );
			Job job = processJobNode( jobNode );
			result.add( job );
		}
		
		return result;
	}
	
	private Job processJobNode( JSONObject job )
	{
		String name = JsonUtils.getRequiredStringValue( job, "name" );
		Job jobImpl = new Job( name, this.baseProperties );
		
		//
		// Properties
		//
		JSONObject properties = (JSONObject)job.get( "properties" );
		if( properties != null )
		{
			for( Object key : properties.keySet() )
			{	
				String value = properties.get( key ).toString();
				jobImpl.setProperty( key.toString(), value );
				
				// Substitute the '.' character for any '_' character within the key name.
				// As JSON doesn't allow the '.' character in field names, the substitution provides
				// us with a way to override system properties (e.g. "mips_outdir": "c:/temp")
				String processedKey = key.toString().replace( '_', '.' );
				jobImpl.setProperty( processedKey, value );
			}
		}
		
		//
		// Tasks
		//
		JSONArray taskArray = (JSONArray)job.get( "tasks" );
		for( int i = 0 ; i < taskArray.size() ; ++i )
		{
			JSONObject taskNode = (JSONObject)taskArray.get( i );
			String taskClass = JsonUtils.getRequiredStringValue( taskNode, "taskid" );
			String taskName = JsonUtils.getOptionalStringValue( taskNode, "name", taskClass );
			
			JSONObject taskParams = 
				taskNode.containsKey( "parameters" ) ? (JSONObject)taskNode.get( "parameters" ) : 
				                                       new JSONObject();
			
			
			ITask taskImpl = taskFactory.createTask( taskClass );
			jobImpl.addTask( taskName, taskImpl, taskParams );
		}
		
		return jobImpl;
	}
	
	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
	/**
	 * Convenience method to create a set of base job properties populated with the master MIPS 
	 * properties contained within the {@link Configuration} object.
	 * 
	 * @param configuration the {@link Configuration} instance to inherit the system level '
	 *                      properties from
	 * @return a {@link Properties} instance that can be used as the base job-level properties
	 *         when creating a {@link JobBuilder}
	 */
	public static Properties makeBaseProperties( Configuration configuration )
	{
		Properties baseProperties = new Properties();
		
		//
		// Important properties from Configuration. These values are inserted separately as they
		// only exist within the internal Configuration.properties member if:
		// a) the user has specified them on the command line
		// b) they exist in one of the properties files that are loaded
		//
		baseProperties.put( Configuration.KEY_MIPS_APPLICATIONDIR, 
		                    configuration.getMipsApplicationDir().getAbsolutePath() );
		baseProperties.put( Configuration.KEY_MIPS_JOBFILE, 
		                    configuration.getJobFile().getAbsolutePath() );
		baseProperties.put( Configuration.KEY_MIPS_OUTPUTDIR, 
		                    configuration.getMipsOutputDir().getAbsolutePath() );
		baseProperties.put( Configuration.KEY_MIPS_PROFILEDIR,
		                    configuration.getMipsProfileDir().getAbsolutePath() );
		
		//
		// Inherit all of the system properties
		//
		for( String key : configuration.properties.stringPropertyNames() )
			baseProperties.put( key, configuration.properties.getProperty(key) );
		
		// Set generic working dir for jobs ($mips.outdir/$jobname)
		String workingDir = "${" + Configuration.KEY_MIPS_OUTPUTDIR + "}/" + 
		                    "${" + Job.PROP_JOBNAME + "}";
		baseProperties.setProperty( Job.PROP_WORKINGDIR, workingDir );
		
		return baseProperties;
	}
}
