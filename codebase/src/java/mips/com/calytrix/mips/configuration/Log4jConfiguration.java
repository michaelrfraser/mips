/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.configuration;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.ConsoleAppender;
import org.apache.logging.log4j.core.appender.FileAppender;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.layout.PatternLayout;

/**
 * Allows a user to define the basic properties for a Log4j configuration and then tell it
 * to apply itself to the logger with the same name as defined in the "appName".
 */
public class Log4jConfiguration
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------
	private static final String DEFAULT_PATTERN_REG  = "%d{HH:mm:ss.SSS} [%t] %-5level %logger{36}: %msg%n";
	private static final String DEFAULT_PATTERN_DATE = "%d{dd MMM yyyy @ HH:mm:ss.SSS} [%t] %-5level %logger{36}: %msg%n";

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private String appName;
	private String pattern;
	private Level level;
	private boolean consoleOn;
	private boolean fileOn;
	private File file;

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	public Log4jConfiguration( String appName )
	{
		this.appName = appName;
		if( appName == null )
			this.appName = LogManager.ROOT_LOGGER_NAME;
		
		this.pattern = DEFAULT_PATTERN_REG;
		this.level = Level.INFO;
		this.consoleOn = true;
		this.fileOn = true;
		this.file = new File( "logs/mips.log" );
	}

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------

	////////////////////////////////////////////////////////////////////////////////////////////
	/// Configuration Activation Methods    ////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	/** Take the configuration values and make them active for the specified root logger */
	public void activateConfiguration()
	{
		// create a new config object and get the context (we'll need it later)
		// once we're done configuring the config we'll add it to the context and then
		// tell it to refresh itself
		LoggerConfig config = new LoggerConfig( appName, Level.OFF, true );
		LoggerContext context = (LoggerContext)LogManager.getContext( false );
		
		// remove any existing appenaders
		Map<String,Appender> appenders = new HashMap<String,Appender>( config.getAppenders() );
		for( String key : appenders.keySet() )
			config.removeAppender( key );

		// create the new appenders and attach them
		if( this.consoleOn )
		{
			ConsoleAppender appender = createConsoleAppender();
			appender.start();
			config.addAppender( appender, level, null );
		}
		
		if( this.fileOn )
		{
			FileAppender appender = createFileAppender( context );
			appender.start();
			config.addAppender( appender, level, null );
		}

		// set the logger level
		config.setLevel( this.level );
		
		// flush the update
		context.getConfiguration().addLogger( appName, config );
		context.updateLoggers();
	}
	

	private ConsoleAppender createConsoleAppender()
	{
		// if we are using the default pattern, 
		// if we are using a custom pattern, this will just fall through
		// if we are using a custom pattern, just use it as is
		if( this.pattern.equals(DEFAULT_PATTERN_REG) )
		{
			
		}
		
		PatternLayout layout = PatternLayout.newBuilder().withPattern(this.pattern).build();
		return ConsoleAppender.createAppender( layout,
		                                       null,                      // filter
		                                       "SYSTEM_OUT",              // targetString
		                                       appName+"-"+"Console",     // name
		                                       null,                      // "follow"
		                                       "false" );                 // ignoreExceptions
	}
	
	private FileAppender createFileAppender( LoggerContext context )
	{
		PatternLayout layout = PatternLayout.newBuilder().withPattern(this.pattern).build();
		return FileAppender.createAppender( file.getAbsolutePath(),       // fileName
		                                    "false",                      // append
		                                    "false",                      // locking
		                                    appName+"-"+"File",           // name
		                                    "true",                       // immediateFlush
		                                    "false",                      // ignoreExceptions
		                                    "true",                       // bufferedIo
		                                    "8192",                       // bufferSizeStr
		                                    layout,                       // layout
		                                    null,                         // filter
		                                    "false",                      // advertise
		                                    null,                         // advertiseUri
		                                    context.getConfiguration() ); // configuration
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Accessor and Mutator Methods ///////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////

	public String getAppName() { return this.appName; }
	public void setAppName( String name ) { this.appName = name; }
	
	public boolean isConsoleOn() { return this.consoleOn; }
	public void setConsoleOn( boolean on ) { this.consoleOn = on; }
	
	public boolean isFileOn() { return this.fileOn; }
	public void setFileOn( boolean on ) { this.fileOn = on; }
	
	public File getFile() { return this.file; }
	public void setFile( File file ) { this.file = file; }
	public void setFile( String path ) { this.file = new File(path); }
	
	public String getLevel() { return this.level.name(); }
	public void setLevel( String level ) { this.level = Level.valueOf(level); }

	public String getPattern() { return this.pattern; }
	public void setPattern( String pattern ) { this.pattern = pattern; }
	
	public boolean isLogWithDate() { return this.pattern.equals(DEFAULT_PATTERN_DATE); }
	/** This will cause the logger to include the date in each message prefix. Note that if you
	    are using a custom pattern, or this value is set after the configuration is applied, this
	    call will have no effect */
	public void setLogWithDate( boolean extended )
	{
		// if we are using a custom pattern, just ignore this
		if( this.pattern.equals(DEFAULT_PATTERN_REG) == false &&
			this.pattern.equals(DEFAULT_PATTERN_DATE) == false )
			return;
		
		if( extended )
			this.pattern = DEFAULT_PATTERN_DATE;
		else
			this.pattern = DEFAULT_PATTERN_REG;
	}
	
	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
}