/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of mips.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.configuration;

import com.calytrix.mips.Messages;

/**
 * Denotes a class of argument value that can be provided on the MIPS command line
 * <p/>
 * <b>Note:</b> This enumeration is provided to make internationalization of the command line help string
 * easier
 */
public enum ArgumentType
{
	//----------------------------------------------------------
	//                        VALUES
	//----------------------------------------------------------
	Path,
	File,
	String,
	None;

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	@Override
	public String toString()
	{
		String description = "";
		switch( this )
		{
			case Path:
				description = Messages.get( "Argument.type.path" );
				break;
			case File:
				description = Messages.get( "Argument.type.file" );
				break;
			case String:
				description = Messages.get( "Argument.type.string" );
				break;
			case None:
			default:
				break;
		}
		
		return description;
	}

	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
}
