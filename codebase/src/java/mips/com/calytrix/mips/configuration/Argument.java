/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.configuration;

import com.calytrix.mips.Messages;
import static com.calytrix.mips.configuration.Configuration.KEY_LOG_FILE;
import static com.calytrix.mips.configuration.Configuration.KEY_LOG_LEVEL;
import static com.calytrix.mips.configuration.Configuration.KEY_MIPS_JOBFILE;
import static com.calytrix.mips.configuration.Configuration.KEY_MIPS_PROFILEDIR;
import static com.calytrix.mips.configuration.Configuration.KEY_MIPS_OUTPUTDIR;

/**
 * Represents a command line argument passed in to MIPS.
 */
public enum Argument
{
	//----------------------------------------------------------
	//                        VALUES
	//----------------------------------------------------------
	// -------------------------------------------------------------------------------------------------------------------
	//          | Argument       | Config Key            | Default Value | Param Desc            | General Description   |
	// -------------------------------------------------------------------------------------------------------------------
	Help        ( "help",         null,                   null,            ArgumentType.None,     Messages.get("Argument.desc.help") ),
	MipsProfile ( "profiledir",   KEY_MIPS_PROFILEDIR,    null,            ArgumentType.Path,     Messages.get("Argument.desc.profiledir") ),
	MipsOutput  ( "outdir",       KEY_MIPS_OUTPUTDIR,     null,            ArgumentType.Path,     Messages.get("Argument.desc.outdir") ),
	ConfigFile  ( "configfile",   null,                   null,            ArgumentType.File,     Messages.get("Argument.desc.configfile") ),
	LogLevel    ( "loglevel",     KEY_LOG_LEVEL,          null,            ArgumentType.String,   Messages.get("Argument.desc.loglevel") ),
	LogFile     ( "logfile",      KEY_LOG_FILE,           null,            ArgumentType.File,     Messages.get("Argument.desc.logfile") ),
	JobFile     ( "jobfile",      KEY_MIPS_JOBFILE,       null,            ArgumentType.File,     Messages.get("Argument.desc.jobfile") );

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private String argumentName;
	private String configProperty;
	private String defaultValue;
	private ArgumentType type;
	private String description;

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	private Argument( String name,
	                  String configProperty,
	                  String defaultValue,
	                  ArgumentType type,
	                  String description )
	{
		this.argumentName = name;
		this.configProperty = configProperty;
		this.defaultValue = defaultValue;
		this.type = type;
		this.description = description;
	}

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	public String getArgumentName()
	{
		return this.argumentName;
	}
	
	public String getConfigProperty()
	{
		return this.configProperty;
	}

	public String getDescription()
	{
		return this.description;
	}

	public boolean isDocumented()
	{
		return this.description != null && !this.description.trim().equals("");
	}

	/** Returns true if this argument expects a parameter. We tell this by looking at the
	    parameter description. If it's null or empty, no parameter is expected, otherwise,
	    one parameter is expected */
	public boolean requiresParam()
	{
		if( this.type == ArgumentType.None )
			return false;
		else
			return true;
	}
	
	public String getDefaultValue()
	{
		return this.defaultValue;
	}
	
	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
	/** Argument must come in the form "--argumentName" */
	public static Argument getArgument( String argument ) throws ConfigurationException
	{
		if( argument.startsWith("--") == false )
		{
			String message = String.format( Messages.get("Argument.error.noprefix"), argument );
			throw new ConfigurationException( message );
		}
		
		argument = argument.substring(2).trim();
		for( Argument potential : Argument.values() )
		{
			if( potential.argumentName.equals(argument) )
				return potential;
		}
		
		String message = String.format( Messages.get("Argument.error.unknown"), argument );
		throw new ConfigurationException( message );
	}

	/**
	 * Returns a formatted string that can be printed to the command line outlining the various
	 * arguments available and their use.
	 */
	public static String getCommandLineHelp()
	{
		int longestName = 0;
		for( Argument argument : Argument.values() )
		{
			if( argument.isDocumented() && (argument.getArgumentName().length() > longestName) )
				longestName = argument.getArgumentName().length();
		}

		// header
		StringBuilder builder = new StringBuilder();
		builder.append( Messages.get("Argument.usage") );
		builder.append( "\n" );
		
		// argument information
		String formatString = "    %-"+(longestName+2)+"s  %10s   %s\n";
		for( Argument argument : Argument.values() )
		{
			if( argument.isDocumented() == false )
				continue;

			String string = String.format( formatString,
			                               "--"+argument.argumentName,
			                               argument.type.toString(),
			                               argument.description );
			builder.append( string );
		}

		return builder.toString();
	}
}
