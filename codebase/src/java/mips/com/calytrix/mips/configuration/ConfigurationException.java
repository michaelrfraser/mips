/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.configuration;

import com.calytrix.mips.MipsException;

public class ConfigurationException extends MipsException
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------
	private static final long serialVersionUID = 98121116105109L;

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	/**
	 * Just create an empty exception
	 */
	public ConfigurationException()
	{
		super();
	}

	/**
	 * @param message The message to create the exception with
	 */
	public ConfigurationException( String message )
	{
		super( message );
	}

	/**
	 * @param cause The cause of the exception
	 */
	public ConfigurationException( Throwable cause )
	{
		super( cause );
	}

	/**
	 * @param message The message to create the exception with
	 * @param cause The cause of the exception
	 */
	public ConfigurationException( String message, Throwable cause )
	{
		super( message, cause );
	}

	/**
	 * @param formatString A format string to use with the arguments to construct the message
	 * @param args The arguments to use with the format string
	 */
	public ConfigurationException( String formatString, Object... args )
	{
		super( String.format( formatString, args ) );
	}

	/**
	 * @param cause The cause of the exception
	 * @param formatString A format string to use with the arguments to construct the message
	 * @param args The arguments to use with the format string
	 */
	public ConfigurationException( Throwable cause, String formatString, Object... args )
	{
		super( String.format( formatString, args ), cause );
	}

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------

	////////////////////////////////////////////////////////////////////////////////////////////
	/// Accessor and Mutator Methods   /////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////

	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
}
