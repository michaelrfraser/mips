/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.configuration;

import java.io.File;
import java.net.URL;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class represents Model Import Pipeline Service configuration data. This object contains a 
 * set of defaults which are then overridden by:
 * <ol>
 * 	<li>Values from the internal configuration defaults resource (see com/calytrix/mips/mips.properties)<li>
 *  <li>Values from the configuration file on the file system</li>
 *  <li>Values specified on the command line</li>
 * </ol>
 * 
 * The static methods of this class allow for a {@link Configuration} object to be parsed from
 * a command line, generating an exception if there is a parsing or validation problem.
 * 
 * How Configuration Loading and Reading Works
 * --------------------------------------------
 * Configuration is stored in an internal `Properties` object. All configuration properties are
 * defined by a key, which is the name they will be declared under in the configuration file.
 * Keys are stored as static String objects with the "KEY_" prefix.
 * 
 * When calling a getXxx() method, the method will look into the properties object using the
 * appropriate key. It will also pass a fallback value, so if you want to know the default for
 * a particular config value, look at its get() method.
 * 
 * Command line arguments should override everything. This is done in the {@link Configurator}
 * by first loading them into a Properties object. The configuration file is then loaded, and 
 * finally the data from the command line is inserted over the top of existing data using the
 * {@link Configuration#override(Properties)} method.
 * 
 * Anything not specified in the configuration file or on the command line will not be present
 * in the internal property set, and as such, the relevant `getXxx()` method will fall back to
 * its default.
 * 
 * Important Filesystem Structure
 * --------------------------------
 * The following locations are major parts of the MIPS structure and represented by different
 * properties. The list below outlines that structure and those properties:
 * 
 * ${mips.home}
 * ${mips.data}
 * 
 */
public class Configuration
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------
	public static final String KEY_MIPS_APPLICATIONDIR = "mips.appdir";
	public static final String KEY_MIPS_PROFILEDIR     = "mips.profiledir";
	public static final String KEY_MIPS_OUTPUTDIR      = "mips.outdir";
	public static final String KEY_MIPS_JOBFILE        = "mips.jobfile";
	public static final String KEY_PWD                 = "mips.pwd";
	
	public static final String KEY_LOG_LEVEL = "mips.loglevel";
	public static final String KEY_LOG_FILE  = "mips.logfile";
	public static final String KEY_LOG_DATE  = "mips.logdate";

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private Logger applicationLogger;
	private Log4jConfiguration appLoggerConfiguration;
	
	protected Properties properties;

	// Configuration options not in the config file
	private File configurationFile;
	private File profileDir;
	private File outDir;
	private File jobFile;

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	protected Configuration()
	{
		this.applicationLogger = null; // set on first access
		this.appLoggerConfiguration = new Log4jConfiguration( "mips" );
		
		this.properties = new Properties();

		// Configuration options not in the config file
		this.configurationFile = null; // set on first request
		this.profileDir = null;
		this.outDir = null;
		this.jobFile = null;
	}
	
	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------

	/**
	 * **DANGER**
	 * 
	 * Override any existing properties with those from the given set. Primarily used
	 * to load in values from command line arguments (which should override everything).
	 */
	protected void override( Properties properties )
	{
		this.properties.putAll( properties );

		// Reparse any things for the logging. Because this is contained off in its own
		// class, we can't just determine the values for the contained properties when
		// when someone wants to know. The default configuration will just apply and never
		// be updated, so we have to actively look for log file things and push them out
		// of the config file and into the log4j configuration object
		if( properties.containsKey(KEY_LOG_LEVEL) )
			appLoggerConfiguration.setLevel( properties.getProperty(KEY_LOG_LEVEL) );
		if( properties.containsKey(KEY_LOG_FILE) )
			appLoggerConfiguration.setFile( properties.getProperty(KEY_LOG_FILE) );
	}

	/**
	 * **DANGER**
	 * 
	 * Override (without any validity checkes!) the identified configuration property
	 * with the given value.
	 */
	protected void override( String key, String value )
	{
		this.properties.setProperty( key, value );
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	/// Sub-Component Configurations    ////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	protected Log4jConfiguration getLog4jConfiguration()
	{
		return this.appLoggerConfiguration;
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	/// Configuration Outside Config File    ///////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Returns the directory in which the MIPS application is installed.
	 * <p/>
	 * <b>Note:</b> This directory is calculated relative to where the <code>mips.jar</code> library
	 * resides on the classpath. If <code>mips.jar</code> cannot be found on the classpath then the 
	 * current directory is returned as a fallback.
	 * 
	 * @return the directory in which the MIPS application is installed.
	 */
	public File getMipsApplicationDir()
	{
		File appDir = null;

		// Tokenize the classpath using the pathSeparator delimiter
		String classpath = System.getProperty( "java.class.path" );
		String pathSeparator = System.getProperty( "path.separator" );
		StringTokenizer classPathTokenizer = new StringTokenizer( classpath, pathSeparator );

		// Iterate through classpath tokens until one is found that contains the mips jar file
		String mipsJarEntry = null;
		while( classPathTokenizer.hasMoreTokens() )
		{
			String currentEntry = classPathTokenizer.nextToken();
			if( currentEntry.endsWith( "mips.jar" ) )
			{
				mipsJarEntry = currentEntry;
				break;
			}
		}

		if( mipsJarEntry != null )
		{
			File mipsJarFile = new File( mipsJarEntry );

			// Only process the entry if the jar file actually exists, and is not a directory
			if( mipsJarFile.exists() && !mipsJarFile.isDirectory() )
			{
				// Reconstruct the jar file object using the absolute path. Without the absolute
				// path we cannot traverse up the filesystem
				mipsJarFile = new File( mipsJarFile.getAbsolutePath() );

				// The mips jar file is usually stored in ${distroRoot}/lib/mips.jar, so we need
				// to go up two levels of parentage to land at the distroRoot
				File libDirectory = mipsJarFile.getParentFile();
				if( libDirectory != null )
					appDir = libDirectory.getParentFile();
			}
		}

		if( appDir == null )
		{
			// Default to current directory if no valid MIPS distro was found 
			String currentDirPath = System.getProperty( "user.dir" );
			appDir = new File( currentDirPath );
		}

		return appDir;
	}
	
	/**
	 * Returns the MIPS user preferences directory.
	 * <p/> 
	 * This directory is where user-specific MIPS configuration can be stored and where the MIPS log file 
	 * will output to by default.
	 * <p/>
	 * <b>Note:</b> the directory is derived from the configuration property {@link #KEY_MIPS_PROFILEDIR}
	 * which can be set via the command line argument {@link Argument#MipsProfile}. If the property is
	 * not present then an OS-specific default directory is used instead.
	 * 
	 * @return the MIPS user preferences directory.
	 * 
	 * @see #getOsDefaultProfileDir()
	 */
	public File getMipsProfileDir()
	{
		if( this.profileDir != null )
			return this.profileDir;
		
		File profileDir = null;
		if( properties.containsKey(KEY_MIPS_PROFILEDIR) )
			profileDir = new File( this.getProperty(KEY_MIPS_PROFILEDIR) );
		else
			profileDir = getOsDefaultProfileDir();
		
		return profileDir;
	}
	
	/**
	 * Sets the MIPS user preferences directory.
	 * <p/> 
	 * This directory is where user-specific MIPS configuration is stored and where the MIPS log file will
	 * output to by default.
	 * 
	 * @param profileDir the MIPS user preferences directory.
	 * 
	 * @see #getOsDefaultProfileDir()
	 */
	public void setMipsProfileDir( File profileDir )
	{
		this.profileDir = profileDir;
	}
	
	/**
	 * Returns the MIPS output directory.
	 * <p/> 
	 * This directory is where MIPS jobs should generate their output to by default.
	 * <p/>
	 * <b>Note:</b> the directory is derived from the configuration property {@link #KEY_MIPS_OUTPUTDIR}
	 * which can be set via the command line argument {@link Argument#MipsOutput}. If the property is
	 * not present then the current directory is used instead.
	 * 
	 * @return the MIPS output directory.
	 */
	public File getMipsOutputDir()
	{
		// if it came in as a command line argument we will already have it loaded
		if( this.outDir != null )
			return outDir;
		
		// do we have a configured value already?
		File datadir = null;
		if( properties.containsKey(KEY_MIPS_OUTPUTDIR) )
			datadir = new File( this.getProperty(KEY_MIPS_OUTPUTDIR) );
		else
			datadir = new File( System.getProperty("user.dir") );
		
		return datadir;
	}
	
	/**
	 * Sets the MIPS output directory.
	 * <p/> 
	 * This directory is where MIPS jobs should generate their output to by default.
	 * 
	 * @param outDir the MIPS output directory.
	 */
	public void setMipsOutputDir( File outDir )
	{
		this.outDir = outDir;
	}

	/**
	 * Returns the location for the configuration file. Note that this can indeed point to a file
	 * that does not exist. If this is the case, no configuration file will be loaded and the 
	 * default settings (plus any command line arguments) will be applied.
	 */
	public File getConfigurationFile()
	{
		if( this.configurationFile != null )
			return this.configurationFile;
		else
			this.configurationFile = new File( getMipsProfileDir(), "mips.config" );
		
		return this.configurationFile;
	}

	public void setConfigurationFile( File file ) throws ConfigurationException
	{
		this.configurationFile = file;
	}
	
	/**
	 * Returns the file that MIPS will read job descriptions from.
	 * <p/>
	 * <b>Note:</b> the file is derived from the configuration property {@link #KEY_MIPS_JOBFILE}
	 * which can be set via the command line argument {@link Argument#JobFile}. If the property is
	 * not present then the file <code>${PWD}/job.mips</code> is used instead.
	 * 
	 * @return the MIPS job file
	 */
	public File getJobFile()
	{
		if( this.jobFile != null )
			return this.jobFile;
		
		File jobFile = null;
		if( properties.containsKey(KEY_MIPS_JOBFILE) )
			jobFile = new File( this.getProperty(KEY_MIPS_JOBFILE) );
		else
			jobFile = new File( System.getProperty("user.dir"), "job.mips" );
		
		return jobFile;
	}

	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Accessor and Mutator Methods ///////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Returns the value of the given property key, performing any property token substitution
	 * as necessary.
	 * <p/>
	 * E.g. if the corresponding value contains "${mips.outdir}/myfile.txt" then the token 
	 * ${mips.outdir} will be replaced with the actual value of the mips.outdir property.
	 * 
	 * @param key the key of the property to return the value for
	 * @return the corresponding value stored against the key, or <code>null</code> if no value
	 *         has been stored against the key
	 */
	public String getProperty( String key )
	{
		String value = this.properties.getProperty( key );
		int iterationCount = 0;
		while( value.contains("${") && iterationCount++ < 16 )
		{
			for( String substKey : this.properties.stringPropertyNames() )
				value = value.replace( "${" + substKey + "}", this.properties.getProperty(substKey) );
		}
		
		// These keys may not be in this.properties depending on whether they have been overriden from
		// defaults
		value = value.replace( "${" + KEY_MIPS_APPLICATIONDIR + "}", getMipsApplicationDir().getAbsolutePath() );
		value = value.replace( "${" + KEY_MIPS_PROFILEDIR + "}", getMipsProfileDir().getAbsolutePath() );
		value = value.replace( "${" + KEY_MIPS_JOBFILE + "}", getJobFile().getAbsolutePath() );
		value = value.replace( "${" + KEY_PWD + "}", System.getProperty("user.dir") );
		
		return value;
	}
	
	/**
	 * Returns the value of the given property key, performing any property token substitution
	 * as necessary.
	 * <p/>
	 * E.g. if the corresponding value contains "${mips.outdir}/myfile.txt" then the token 
	 * ${mips.outdir} will be replaced with the actual value of the mips.outdir property.
	 * 
	 * @param key the key of the property to return the value for
	 * @return the corresponding value stored against the key, or <code>null</code> if no value
	 *         has been stored against the key
	 */
	public String getProperty( String key, String defaultValue )
	{
		String value = defaultValue;
		if( this.properties.containsKey(key) )
			value = this.properties.getProperty( key );
		
		int iterationCount = 0;
		while( value.contains("${") && iterationCount++ < 16 )
		{
			for( String substKey : this.properties.stringPropertyNames() )
				value = value.replace( "${" + substKey + "}", this.properties.getProperty(substKey) );
		}
		return value;
	}
	
	/** Get the MIPS application logger. Major components will create a logger that sits below
	    this one. Configuration for the logger is as per {@link #getLog4jConfiguration()}. */
	public Logger getMipsLogger()
	{
		if( this.applicationLogger != null )
			return applicationLogger;
		
		this.appLoggerConfiguration.activateConfiguration();
		this.applicationLogger = LogManager.getFormatterLogger( "mips" );
		return applicationLogger;
	}
	
	private File getOsDefaultProfileDir()
	{
		if( System.getProperty("os.name").contains("indows") )
		{
			File documentsDir = new File( System.getProperty("user.home"), "Documents" );
			return new File( documentsDir, "MIPS" );
		}
		else
		{
			return new File( System.getProperty("user.home"), ".mips" );
		}
	}
	
	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
	////////////////////////////////////////////////////////////////////////////////////////////
	/// System Properties    ///////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Load the properties file "build.properties" into the system properties and return
	 * the value of the value of "${build.version} (build ${build.number})"
	 */
	public static String getVersion()
	{
		Properties properties = new Properties();
		if( System.getProperty("build.version") == null )
		{
			try
			{
				URL url = ClassLoader.getSystemResource( "build.properties" );
				properties.load( url.openStream() );
			}
			catch( Exception e )
			{
				// do nothing, not much we can do
			}
		}
		
		// Get the build number
		String buildVersion = properties.getProperty( "build.version", "unknown" );
		String buildNumber = properties.getProperty( "build.number", "unknown" );
		return buildVersion + " (build "+buildNumber+")";
	}
	
}
