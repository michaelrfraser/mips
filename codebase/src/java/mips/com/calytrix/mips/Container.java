/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips;

import org.apache.logging.log4j.Logger;

import com.calytrix.mips.configuration.Configuration;
import com.calytrix.mips.configuration.ConfigurationException;
import com.calytrix.mips.configuration.Configurator;
import com.calytrix.mips.job.ExecutionManager;
import com.calytrix.mips.task.TaskFactory;

/**
 * Every instance of Master Import Pipeline Service is started inside a unique Container. This encapsulates
 * a complete MIPS runtime
 */
public class Container
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private Configuration configuration;
	protected Logger logger;
	private long startTime; // when the application started up
	private volatile boolean started;
	
	// Execution Management
	private ExecutionManager executionManager;
	
	// Task Management
	private TaskFactory taskFactory;

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	public Container( Configuration configuration ) throws ConfigurationException
	{
		this.started = false;
		this.configuration = configuration;
		this.logger = this.configuration.getMipsLogger();
		
		// NOTE: Manager Modules
		//       To avoid situations where subcomponents try to get references to others
		//       while we are still in the construction phase, we separate the creation
		//       step and the configuration step. Major initialization of the subcomponents
		//       should only happen in configure() as a group all at the end
		
		// Execution Management
		this.executionManager = new ExecutionManager( this.logger );

		// Task Factory
		this.taskFactory = new TaskFactory();
		
		// configure the modules - now that we have all the links set up,
		// let's tell the modules to do their initialization and configuration
		this.executionManager.configure( configuration );
	}

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------

	////////////////////////////////////////////////////////////////////////////////////////////
	/// Lifecycle Methods    ///////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	public void startup() throws MipsException
	{
		this.startTime = System.currentTimeMillis();

		try
		{
			this.executionManager.startup();
		}
		catch( MipsException owse )
		{
			// something failed, but we won't know where, so we need to shut
			// everything down and then bounce.
			logger.fatal( Messages.get("Container.starterror") );
			try { this.executionManager.shutdown();   } catch( Exception | Error e ) {}
			throw owse;
		}
		
		this.started = true;
	}

	public void shutdown()
	{
		// write out the web overrides
		try { Configurator.shutdown(configuration); } catch( Exception | Error e ) { logger.error(e.getMessage(),e); }
		
		// tell everything else to shut down
		try { this.executionManager.shutdown();      } catch( Exception | Error e ) { logger.error(e.getMessage(),e); }
		
		this.started = false;
		
		synchronized( this )
		{
			this.notifyAll();
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	/// Accessor and Mutator Methods   /////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	public boolean isStarted()
	{
		return this.started;
	}
	
	public Configuration getConfiguration()
	{
		return this.configuration;
	}

	public ExecutionManager getExecutionManager()
	{
		return this.executionManager;
	}
	
	public TaskFactory getTaskFactory()
	{
		return this.taskFactory;
	}
	
	public long getUptimeMillis()
	{
		return System.currentTimeMillis() - startTime;
	}
	
	public Logger getLogger()
	{
		return this.logger;
	}

	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
}
