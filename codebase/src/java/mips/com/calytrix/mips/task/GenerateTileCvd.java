/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.task;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;

import com.calytrix.mips.Messages;
import com.calytrix.mips.MipsException;
import com.calytrix.mips.job.ExecutionManager;
import com.calytrix.mips.job.Job;
import com.calytrix.mips.utils.StringUtils;
import com.calytrix.mips.utils.XmlUtils;

/**
 * A sample {@link ITask} implementation that fetches a resource from URL and saves it to a location on 
 * the filesystem.
 * 
 * <h3>System Properties</h3>
 * <table>
 *  <tr><th>Property Name</th> <th>Description</th></tr>
 *  <tr><td>curl.execwin</td>  <td>The location of the curl executable under Windows</td></tr>
 * </table>
 * 
 * <h3>Task Parameters</h3>
 * <table>
 *  <tr><th>Parameter Name</td>   <th>Required</th>   <th>Description</th></tr>
 *  <tr><td>url</td>              <td>Yes</td>        <td>The URL of the resource to fetch</td></tr>
 *  <tr><td>outfile</td>          <td>No</td>         <td>The file to output the fetched resource to</td></tr>
 * </table> 
 */
public class GenerateTileCvd implements ITask
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------
	//
	// System properties for the task
	//
	private static final String PROP_EXEC = "titan.exec";
	
	//
	// Per-task parameters
	//
	private static final String PARAM_NAME     = "name";
	private static final String PARAM_PLANET   = "planet";
	private static final String PARAM_IMAGE    = "image";
	private static final String PARAM_METADATA = "metadata";
	private static final String PARAM_OUTDIR   = "outdir";

	private static final String XPATH_GEOTRANSFORM = "/PAMDataset/GeoTransform";
	private static final String TEMPLATE_IMPORTJS  = "com/calytrix/mips/task/GenerateTileCvd.import.js.template";
	private static final String TEMPLATE_IMPORTCFG = "com/calytrix/mips/task/GenerateTileCvd.import.cfg.template";
	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private File titanExec;
	private Job context;
	
	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	private ImageMetadata gatherMetadata( String name, File imageFile, File auxFile ) throws MipsException
	{
		int width = 0;
		int height = 0;
		try
		{
			BufferedImage image = ImageIO.read( imageFile );
			width = image.getWidth();
			height = image.getHeight();
		}
		catch( IOException ioe )
		{
			String message = String.format( Messages.get("GenerateTileCvd.cantreadimage"), 
			                                imageFile.getAbsolutePath(), 
			                                ioe.getLocalizedMessage() );
			throw new MipsException( message, ioe );
		}
		
		Document metadataDom = XmlUtils.readFile( auxFile );
		String geoTransformRaw = XmlUtils.queryStringValue( metadataDom, XPATH_GEOTRANSFORM );
		String[] geoTransformTokens = geoTransformRaw.split( "," );
		if( geoTransformTokens.length != 6 )
			throw new MipsException( Messages.get("GenerateTileCvd.invalidgeotransform") );
		
		double[] geoTransform = new double[6];
		for( int i = 0 ; i < 6 ; ++i )
			geoTransform[i] = Double.valueOf( geoTransformTokens[i] );
		
		double tlLon = geoTransform[0];
		double tlLat = geoTransform[3];
		double brLon = tlLon + (width * geoTransform[1]) + (height * geoTransform[2]);
		double brLat = tlLat + (width * geoTransform[4]) + (height * geoTransform[5]);
		
		return new ImageMetadata( name,
		                          imageFile,
		                          width, 
		                          height, 
		                          Math.min(tlLat, brLat), 
		                          Math.min(tlLon, brLon), 
		                          Math.max(tlLat, brLat),
		                          Math.max(tlLon, brLon) );
	}
	
	private File createImportScript( ImageMetadata metadata )
	{
		String template = StringUtils.getResourceString( TEMPLATE_IMPORTJS );
		template = template.replace( "#LATMIN#", String.valueOf(metadata.getLatMin()) );
		template = template.replace( "#LONMIN#", String.valueOf(metadata.getLonMin()) );
		template = template.replace( "#LATMAX#", String.valueOf(metadata.getLatMax()) );
		template = template.replace( "#LONMAX#", String.valueOf(metadata.getLonMax()) );
		template = template.replace( "#TEXW#", String.valueOf(metadata.getWidth()) );
		template = template.replace( "#TEXH#", String.valueOf(metadata.getHeight()) );
		template = template.replace( "#TEXH#", String.valueOf(metadata.getHeight()) );
		template = template.replace( "#NAME#", String.valueOf(metadata.getName()) );
		
		template = template.replace( "#IMGPATH#", StringUtils.getNormalizedFilePath(metadata.getFile()) );
		
		File outFile = new File( context.getWorkingDirectory(), "import.js" );
		try
		{
			FileWriter writer = new FileWriter( outFile );
			writer.write( template );
			writer.close();
			return outFile;
		}
		catch( IOException ioe )
		{
			throw new MipsException( ioe );
		}
	}
	
	private File createImportConfig( ImageMetadata metadata, 
	                                 File scriptFile, 
	                                 String planetName, 
	                                 File outDir )
	{
		String template = 
			StringUtils.getResourceString( TEMPLATE_IMPORTCFG );
		
		File imageSrcDir = metadata.getFile().getParentFile();
		template = template.replace( "#PLANET#", planetName );
		template = template.replace( "#WADDIR#", StringUtils.getNormalizedFilePath(outDir) );
		template = template.replace( "#IMPORTSCRIPT#", StringUtils.getNormalizedFilePath(scriptFile) );
		template = template.replace( "#SRCDIR#", StringUtils.getNormalizedFilePath(imageSrcDir) );
		
		File outFile = new File( context.getWorkingDirectory(), "import.cfg" );
		try
		{
			FileWriter writer = new FileWriter( outFile );
			writer.write( template );
			writer.close();
			return outFile;
		}
		catch( IOException ioe )
		{
			throw new MipsException( ioe );
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// ITask Interface /////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void configure( Job context ) throws Exception
	{
		this.context = context;
		
		String titanExecPath = context.getProperty( PROP_EXEC );
		File titanExec = new File( titanExecPath );
		if( !titanExec.exists() )
		{
			throw new MipsException( String.format( Messages.get("titan.execnotfound"), 
			                                        titanExec.getAbsolutePath()) );
		}
		this.titanExec = titanExec;
	}
	
	@Override
	public void execute( JSONObject parameters, Logger logger ) throws Exception
	{
		if( titanExec == null )
			throw new MipsException( Messages.get("titan.noexec") );
		
		if( !parameters.containsKey(PARAM_IMAGE) )
			throw new MipsException( Messages.get("GenerateTileCvd.noimage") );
		
		//
		// Process task parameters
		//
		
		// Source Image
		String imagePath = parameters.get( PARAM_IMAGE ).toString();
		imagePath = context.substitutePropertyTokens( imagePath );
		File imageFile = new File( imagePath );
		if( !imageFile.exists() )
		{
			throw new MipsException( String.format( Messages.get("GenerateTileCvd.imagenotfound"), 
			                                        imageFile.getAbsolutePath()) );
		}
		
		// Geospatial Metadata
		String metadataPath = imagePath + ".aux.xml";
		if( parameters.containsKey(PARAM_METADATA) )
		{
			metadataPath = parameters.get( PARAM_METADATA ).toString();
			metadataPath = context.substitutePropertyTokens( metadataPath );
		}
		
		File metadataFile = new File( metadataPath );
		if( !metadataFile.exists() )
		{
			throw new MipsException( String.format( Messages.get("GenerateTileCvd.metadatanotfound"), 
			                                        imageFile.getAbsolutePath()) );
		}
		
		// Dataset Name
		String name = "MipsImport";
		if( parameters.containsKey(PARAM_NAME) )
			name = context.substitutePropertyTokens( parameters.get(PARAM_NAME).toString() );
		
		// Planet Name
		String planet = "earth7";
		if( parameters.containsKey(PARAM_PLANET) )
			planet = context.substitutePropertyTokens( parameters.get(PARAM_PLANET).toString() );
		
		// Output Directory
		String outDirPath = "_cvd";
		if( parameters.containsKey(PARAM_OUTDIR) )
			outDirPath = context.substitutePropertyTokens( parameters.get(PARAM_OUTDIR).toString() );
		
		File outDir = new File( outDirPath );
		if( !outDir.isAbsolute() )
			outDir = new File( context.getWorkingDirectory(), outDirPath );
		
		outDir.mkdirs();
		
		//
		// Generate import.js and import.cfg 
		//
		ImageMetadata metadata = gatherMetadata( name, imageFile, metadataFile );
		File importScript = createImportScript( metadata );
		File importConfig = createImportConfig( metadata, importScript, planet, outDir );
		
		//
		// Execute Titan/mappressor using the generated import.cfg
		//
		List<String> arguments = new ArrayList<>();
		arguments.add( "-raster" );
		arguments.add( importConfig.getAbsolutePath() );
		
		// Spawn the process
		Process process = ExecutionManager.spawnProcess( titanExec,
		                                                 arguments, 
		                                                 context.getWorkingDirectory() );
		
		// Wait for the process to finish executing, if it returns a non-zero exit code then a 
		// MipsException will be thrown
		ExecutionManager.waitForProcess( process, titanExec );
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Accessor and Mutator Methods ///////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	
	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
	private class ImageMetadata
	{
		private String name;
		private File file;
		private int width;
		private int height;
		private double latMin;
		private double lonMin;
		private double latMax;
		private double lonMax;
		
		public ImageMetadata( String name,
		                      File file,
		                      int width, 
		                      int height, 
		                      double latMin, 
		                      double lonMin, 
		                      double latMax, 
		                      double lonMax )
		{
			this.name = name;
			this.file = file;
			this.width = width;
			this.height = height;
			this.latMin = latMin;
			this.lonMin = lonMin;
			this.latMax = latMax;
			this.lonMax = lonMax;
		}
		
		public String getName()
		{
			return name;
		}

		public File getFile()
		{
			return file;
		}
		
		public int getWidth()
		{
			return this.width;
		}
		
		public int getHeight()
		{
			return this.height;
		}
		
		@Override
		public String toString()
		{
			return String.format( "width=%d, height=%d, latMin=%f, lonMin=%f, latMax=%f, lonMax=%f", 
			                      this.width, 
			                      this.height,
			                      this.latMin,
			                      this.lonMin,
			                      this.latMax,
			                      this.lonMax );
		}

		public double getLatMin()
		{
			return latMin;
		}

		public double getLonMin()
		{
			return lonMin;
		}

		public double getLatMax()
		{
			return latMax;
		}

		public double getLonMax()
		{
			return lonMax;
		}
	}
}
