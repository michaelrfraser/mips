/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.task;

import java.util.HashMap;
import java.util.Map;

import com.calytrix.mips.Messages;
import com.calytrix.mips.MipsException;

/**
 * A factory that creates {@link ITask} instances at runtime.
 * <p/>
 * To request creation of a {@link ITask} instance, call {@link #createTask(String)}.
 */
public class TaskFactory
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private Map<String,Class<? extends ITask>> taskClasses;

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	public TaskFactory()
	{
		this.taskClasses = new HashMap<>();
		this.registerTask( Curl.class );
		this.registerTask( Wait.class );
		this.registerTask( ConvertJpg2k.class );
		this.registerTask( GenerateTileCvd.class );
	}

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	private void registerTask( Class<? extends ITask> task )
	{
		taskClasses.put( task.getCanonicalName(), task );
	}
	
	/**
	 * Creates an instance of an {@link ITask} with the specified class name
	 * 
	 * @param clasz the canonical class name of the {@link ITask} to instantiate
	 * @return an instance of the requested {@link ITask} implementation
	 * @throws MipsException if the specified {@link ITask} implementation has not been registered with
	 *                       this factory
	 */
	public ITask createTask( String clasz ) throws MipsException
	{
		Class<? extends ITask> taskClass = taskClasses.get( clasz );
		if( taskClass != null )
		{
			try
			{
				return taskClass.newInstance();
			}
			catch( Exception e )
			{
				throw new MipsException( e );
			}
		}
		else
		{
			String message = String.format( Messages.get("TaskFactory.error.unknownclass"), clasz );
			throw new MipsException( message );
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Accessor and Mutator Methods ///////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	

	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
}
