/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import com.calytrix.mips.Messages;
import com.calytrix.mips.MipsException;
import com.calytrix.mips.job.ExecutionManager;
import com.calytrix.mips.job.Job;

/**
 * A sample {@link ITask} implementation that fetches a resource from URL and saves it to a location on 
 * the filesystem.
 * 
 * <h3>System Properties</h3>
 * <table>
 *  <tr><th>Property Name</th>        <th>Description</th></tr>
 *  <tr><td>gdal.translate.exec</td>  <td>The location of the gdal_translate executable</td></tr>
 * </table>
 * 
 * <h3>Task Parameters</h3>
 * <table>
 *  <tr><th>Parameter Name</td>   <th>Required</th>   <th>Description</th></tr>
 *  <tr><td>infile</td>           <td>Yes</td>        <td>The path to the JPEG2000 image to convert</td></tr>
 *  <tr><td>outformat</td>        <td>No</td>         <td>The format to convert the JPEG2000 to (default PNG)</td></tr>
 *  <tr><td>outdir</td>           <td>No</td>         <td>The path to save the conversion artefacts to</td></tr>
 * </table> 
 */
public class ConvertJpg2k implements ITask
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------
	//
	// System properties for the task
	//
	public static final String PROP_EXEC = "gdal.translate.exec";
	
	//
	// Per-task parameters
	//
	public static final String PARAM_SKIPIFEXISTS = "skipifexists";
	public static final String PARAM_INFILE = "infile";
	public static final String PARAM_OUTDIR = "outdir";

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private File gdalPath;
	private File gdalExec;
	private Job context;
	
	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------

	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// ITask Interface /////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void configure( Job context ) throws Exception
	{
		this.context = context;
		
		String gdalExecPath = context.getProperty( PROP_EXEC );
		File gdalExec = new File( gdalExecPath );
		if( !gdalExec.exists() )
		{
			throw new MipsException( String.format( Messages.get("Gdal.execnotfound"), 
			                                        gdalExec.getAbsolutePath()) );
		}
		this.gdalExec = gdalExec;
		this.gdalPath = gdalExec.getParentFile();
	}
	
	@Override
	public void execute( JSONObject parameters, Logger logger ) throws Exception
	{
		if( gdalExec == null )
			throw new MipsException( Messages.get("Gdal.noexec") );
		
		List<String> arguments = new ArrayList<>();
		
		// Format of the image to output
		arguments.add( "-of" );
		arguments.add( "PNG" );
		
		// The image to convert
		if( !parameters.containsKey(PARAM_INFILE) )
			throw new MipsException( Messages.get("ConvertJpg2k.noinfile") );
		
		String inPath = parameters.get(PARAM_INFILE).toString();
		inPath = context.substitutePropertyTokens( inPath );
		File inFile = new File( inPath );
		arguments.add( inFile.getAbsolutePath() );
		
		// The file to write to
		String outFileName = inFile.getName() + ".png";
		File outFile = new File( context.getWorkingDirectory(), outFileName );
		if( parameters.containsKey(PARAM_OUTDIR) )
		{
			String outDirPath = parameters.get(PARAM_OUTDIR).toString();
			File outDir = context.getPathRelativeTo( outDirPath, context.getWorkingDirectory() );
			outDir.mkdirs();
			outFile = new File( outDir, outFileName );
		}
		
		arguments.add( outFile.getAbsolutePath() );
		
		if( parameters.containsKey(PARAM_SKIPIFEXISTS) && 
			parameters.get(PARAM_SKIPIFEXISTS).equals(Boolean.TRUE) &&
			outFile.exists() )
		{
			logger.info( Messages.get("ConvertJpg2k.skipconvert"),
			             inFile.getAbsolutePath(),
			             outFile.getAbsolutePath() );
			return;
		}
		
		
		
		// Spawn the process
		Process process = ExecutionManager.spawnProcess( gdalExec,
		                                                 arguments, 
		                                                 this.gdalPath );
		
		// Wait for the process to finish executing, if it returns a non-zero exit code then a 
		// MipsException will be thrown
		ExecutionManager.waitForProcess( process, gdalExec );
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Accessor and Mutator Methods ///////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	
	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
}
