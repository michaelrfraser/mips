/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.task;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import com.calytrix.mips.Messages;
import com.calytrix.mips.MipsException;
import com.calytrix.mips.job.ExecutionManager;
import com.calytrix.mips.job.Job;

/**
 * A sample {@link ITask} implementation that fetches a resource from URL and saves it to a location on 
 * the filesystem.
 * 
 * <h3>System Properties</h3>
 * <table>
 *  <tr><th>Property Name</th> <th>Description</th></tr>
 *  <tr><td>curl.execwin</td>  <td>The location of the curl executable under Windows</td></tr>
 * </table>
 * 
 * <h3>Task Parameters</h3>
 * <table>
 *  <tr><th>Parameter Name</td>   <th>Required</th>   <th>Description</th></tr>
 *  <tr><td>url</td>              <td>Yes</td>        <td>The URL of the resource to fetch</td></tr>
 *  <tr><td>outfile</td>          <td>No</td>         <td>The file to output the fetched resource to</td></tr>
 * </table> 
 */
public class Curl implements ITask
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------
	//
	// System properties for the task
	//
	public static final String PROP_EXEC = "curl.exec";
	
	//
	// Per-task parameters
	//
	public static final String PARAM_URL = "url";
	public static final String PARAM_OUTFILE = "outfile";

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private File curlExec;
	private Job context;
	
	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------

	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// ITask Interface /////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void configure( Job context ) throws Exception
	{
		this.context = context;
		
		String curlPath = context.getProperty( PROP_EXEC );
		File curlExec = new File( curlPath );
		if( !curlExec.exists() )
		{
			throw new MipsException( String.format( Messages.get("Curl.execnotfound"), 
			                                        curlExec.getAbsolutePath()) );
		}
		this.curlExec = curlExec;
	}
	
	@Override
	public void execute( JSONObject parameters, Logger logger ) throws Exception
	{
		if( curlExec == null )
			throw new MipsException( Messages.get("Curl.noexec") );
		
		List<String> arguments = new ArrayList<>();
		
		// The URL to fetch
		if( !parameters.containsKey(PARAM_URL) )
			throw new MipsException( Messages.get("Curl.nourl") );
		
		arguments.add( parameters.get(PARAM_URL).toString() );
		
		// The file to write to (optional)
		if( parameters.containsKey(PARAM_OUTFILE) )
		{
			String path = parameters.get(PARAM_OUTFILE).toString();
			path = context.substitutePropertyTokens( path );
			File outputFile = new File( path );
			arguments.add( "-o" );
			arguments.add( outputFile.getAbsolutePath() );
		}
		
		// Spawn the process
		Process process = ExecutionManager.spawnProcess( curlExec,
		                                                 arguments, 
		                                                 context.getWorkingDirectory() );
		
		// Wait for the process to finish executing, if it returns a non-zero exit code then a 
		// MipsException will be thrown
		ExecutionManager.waitForProcess( process, curlExec );
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Accessor and Mutator Methods ///////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	
	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
}
