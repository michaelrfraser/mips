/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.task;

import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import com.calytrix.mips.job.Job;

/**
 * A sample {@link ITask} implementation that waits for a specified time period
 * 
 * <h3>System Properties</h3>
 * None
 * 
 * <h3>Task Parameters</h3>
 * <table>
 *  <tr><th>Parameter Name</td>   <th>Required</th>   <th>Description</th></tr>
 *  <tr><td>period</td>           <td>No</td>         <td>The time to wait in milliseconds.</td></tr>
 * </table> 
 */
public class Wait implements ITask
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------
	//
	// Per-task parameters
	// 
	public static final String PARAM_PERIOD = "period";

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	
	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	
	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------

	////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////// ITask Interface /////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public void configure( Job context ) throws Exception
	{
		
	}
	
	@Override
	public void execute( JSONObject parameters, Logger logger ) throws Exception
	{
		long period = 0;
		if( parameters.containsKey("period") )
			period = ((Number)parameters.get("period")).longValue();
		
		Thread.sleep( period );
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Accessor and Mutator Methods ///////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	
	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
}
