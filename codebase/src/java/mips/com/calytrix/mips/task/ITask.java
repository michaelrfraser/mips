/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.task;

import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import com.calytrix.mips.job.Job;

/**
 * Represents an abstract unit of execution that is completed as part of a {@link Job}.
 * <p/>
 * The life-cycle of a task involves two phases:
 * <ol>
 *  <li>
 *   <b>Configuration:</b> The task is passed the {@link Job} context in which it will execute within.
 *   The task can cache system properties specified in the MIPS configuration file by calling 
 *   {@link Job#getProperty(String)}.
 *  </li>
 *  <li>
 *   <b>Execution:</b> The task's business logic is executed. Individual task parameters are provided
 *   through the <code>parameters</code> argument.
 *  </li> 
 * </ol>
 * Any exceptions thrown while executing the task will cause the containing {@link Job} to fail with no
 * subsequent tasks being executed. 
 */
public interface ITask
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	/**
	 * Provides the {@link ITask} instance a chance to configure itself within the context of the 
	 * {@link Job} that it will eventually execute within.
	 * <p/>
	 * The task can call {@link Job#getProperty(String)} to obtain system properties specified within the
	 * MIPS configuration file, or properties specified within the Job file itself.
	 * 
	 * @param context the {@link Job} that the task will eventually run within
	 * @throws Exception if an error occurred while configuring the task
	 */
	public void configure( Job context ) throws Exception;
	
	/**
	 * Executes this task's business logic.
	 * <p/>
	 * If this method completes without throwing an {@link Exception} then the next {@link ITask} in the
	 * containing {@link Job}'s task list will execute.
	 * <p/>
	 * If this method throws an {@link Exception} then the containing {@link Job} will fail immediately
	 * with no subsequent tasks being executed.
	 *
	 * @param parameters JSON parameters to customize this task's execution
	 * @throws Exception if an error occurred while executing this task's business logic
	 */
	public void execute( JSONObject parameters, Logger logger ) throws Exception;
}
