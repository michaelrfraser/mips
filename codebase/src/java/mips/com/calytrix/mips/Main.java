/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Future;

import org.apache.logging.log4j.Logger;

import com.calytrix.mips.configuration.Argument;
import com.calytrix.mips.configuration.Configuration;
import com.calytrix.mips.configuration.Configurator;
import com.calytrix.mips.configuration.JobBuilder;
import com.calytrix.mips.job.ExecutionManager;
import com.calytrix.mips.job.Job;
import com.calytrix.mips.job.JobResult;

/**
 * Main entry point for the MIPS application
 */
public class Main
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private Container container;

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	private Main()
	{
		this.container = null;
	}

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	private void execute( String[] args )
	{
		List<String> arguments = new ArrayList<>();
		for( String argument : args )
			arguments.add( argument );
		
		
		// Check for the --help command line arg and bail out early if its there
		if( arguments.contains("--help") )
		{
			System.out.println( Argument.getCommandLineHelp() );
			return;
		}

		// parse the command line and get our configuration checked and loaded
		Configuration configuration = Configurator.load( arguments.toArray(new String[]{}) );
			
		// load our Container to kick things off!
		this.container = new Container( configuration );
		container.startup();
		
		//
		// Parse Job File
		//
		Logger logger = this.container.getLogger();
		Properties baseJobProperties = JobBuilder.makeBaseProperties( configuration );
		File jobFile = configuration.getJobFile();
		JobBuilder builder = new JobBuilder( this.container.getTaskFactory(),
		                                     baseJobProperties, 
		                                     logger );
		List<Job> jobs = builder.fromFile( jobFile );
		ExecutionManager execMan = this.container.getExecutionManager();
		
		logger.info( String.format(Messages.get("Main.startupinfo"),
		                           jobs.size(),
		                           execMan.getThreadCount()) );
		
		//
		// Submit Jobs to the ExecutionManager
		//
		List<Future<JobResult>> jobFutures = new ArrayList<>();
		for( Job job : jobs )
		{
			Future<JobResult> future = execMan.submitJob( job );
			jobFutures.add( future );
		}
		
		//
		// Collect Job Results
		//
		List<JobResult> results = new ArrayList<>();
		jobFutures.forEach( (Future<JobResult> f) -> {
			try
			{
				JobResult result = f.get();
				results.add( result );
				
			}
			catch( Exception e )
			{
				// Exceptions should not be thrown by the future as we catch them all in 
				// Job.execute(), so re-throw as a runtime exception so that we know that 
				// something needs to be addressed.
				throw new IllegalStateException( e );
			}
		} );
		
		int successes = 0;
		int failures = 0;
		for( JobResult result : results )
		{
			if( result.isSuccess() )
				++successes;
			else
				++failures;
		}
		
		logger.info( String.format(Messages.get("Main.executioncomplete"), successes, failures) );
		
		container.shutdown();
	}

	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
	public static void main( String[] args )
	{
		new Main().execute( args );
	}
}
