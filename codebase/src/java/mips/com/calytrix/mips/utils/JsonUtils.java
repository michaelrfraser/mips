/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.utils;

import org.json.simple.JSONObject;

import com.calytrix.mips.Messages;
import com.calytrix.mips.MipsException;

/**
 * Helper methods for reading values out of a {@link JSONObject}
 */
public class JsonUtils
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------

	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Accessor and Mutator Methods ///////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////

	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
	/**
	 * Returns the string value stored against the specified member key
	 * 
	 * @param object the {@link JSONObject} that contains the member
	 * @param key the member name of the {@link JSONObject} to return the value for
	 * @return the value of the desired member, interpreted as a String
	 * @throws MipsException if <code>object</code> does not contain the member <code>key</code> 
	 */
	public static String getRequiredStringValue( JSONObject object, String key )
		throws MipsException
	{
		if( object.containsKey(key) )
		{
			return object.get( key ).toString();
		}
		else
		{
			String message = String.format( Messages.get("JsonUtils.missingvalue" ), 
			                                object.toString(), 
			                                key );
			throw new MipsException( message );
		}
	}
	
	/**
	 * Returns the string value stored against the specified member key, or <code>defaultValue</code> if
	 * no such member exists
	 * 
	 * @param object the {@link JSONObject} that contains the member
	 * @param key the member name of the {@link JSONObject} to return the value for
	 * @param defaultValue the value that will be returned if <code>object</code> does not contain the
	 *                     desired member
	 * @return the value of the desired member if it exists, or <code>defaultValue</code> if it does not
	 *         exist 
	 */
	public static String getOptionalStringValue( JSONObject object, 
	                                             String key, 
	                                             String defaultValue )
	{
		if( object.containsKey(key) )
			return object.get( key ).toString();
		else
			return defaultValue;
	}
}
