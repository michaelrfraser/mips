/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.calytrix.mips.Messages;

public class StringUtils
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------
	public static final DateTimeFormatter LONG_DATE = DateTimeFormatter.ofPattern( "eeee d MMMM, u" );

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
	/**
	 * Returns `System.currentTimeMillis()` as seconds since the epoch
	 */
	public static int currentTimeSeconds()
	{
		return (int)TimeUnit.MILLISECONDS.toSeconds( System.currentTimeMillis() );
	}

	/**
	 * Force a sleep for the given amount of milliseconds and consume any exceptions
	 * generated from this so we can call this method in a single line without a big
	 * try/catch block every time we need it.
	 */
	public static void sleep( long millis )
	{
		try
		{
			Thread.sleep( millis );
		}
		catch( Exception e )
		{
			return;
		}
	}

	////////////////////////////////////////////////////////////////////////
	/// Date/Time Utils   //////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////
	public static String formatDateTimeString( long millisSinceEpoch )
	{
		Instant instant = Instant.ofEpochMilli( millisSinceEpoch );
		LocalDateTime datetime = LocalDateTime.ofInstant( instant, ZoneId.systemDefault() );
		return datetime.format( DateTimeFormatter.RFC_1123_DATE_TIME );
	}
	
	public static String formatDateTimeString( LocalDate localdate )
	{
		return localdate.atStartOfDay(ZoneId.systemDefault()).format( LONG_DATE );
	}

	public static String formatTimeBetweenNowAndThen( long millis )
	{
		return formatDuration( System.currentTimeMillis()-millis );
	}
	
	public static String formatDuration( long millisDuration )
	{
		if( millisDuration < 1000 )
		{
			return String.format( Messages.get("StringUtils.durationms"), millisDuration );
		}
		else if( millisDuration < (1000*60) )
		{
			return String.format( Messages.get("StringUtils.durationseconds"), millisDuration/1000 );
		}
		else if( millisDuration < (1000*60*60) )
		{
			int seconds = (int) (millisDuration / 1000) % 60 ;
			int minutes = (int) ((millisDuration / (1000*60)) % 60);
			return String.format( Messages.get("StringUtils.durationminutes"), minutes, seconds );
		}
		else if( millisDuration < (1000*60*60*24) )
		{
			int seconds = (int) (millisDuration / 1000) % 60 ;
			int minutes = (int) ((millisDuration / (1000*60)) % 60);
			int hours   = (int) ((millisDuration / (1000*60*60)) % 24);
			return String.format( Messages.get("StringUtils.durationhours"), hours, minutes, seconds );
		}
		else
		{
			int minutes = (int) ((millisDuration / (1000*60)) % 60);
			int hours   = (int) ((millisDuration / (1000*60*60)) % 24);
			int days    = (int) ((millisDuration / (1000*60*60*24)));
			return String.format( Messages.get("StringUtils.durationdays"), days, hours, minutes );
		}
	}
		

	////////////////////////////////////////////////////////////////////////
	/// String Utils   /////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////
	public static String bytesToString( long bytes )
	{
		int unit = 1000;
		if( bytes < unit )
			return bytes + " B";
		int exp = (int)(Math.log( bytes ) / Math.log( unit ));
		return String.format( "%.1f %sB", bytes/Math.pow(unit,exp), "kMGTPE".charAt(exp-1) );
	}

	/**
	 * Turns the given array of objects into a comma-separated String. If the first value is
	 * `true`, the string will be wrapped in `[]`, otherwise it will not.
	 * 
	 * @param wrapInBrackets Should the string be wrapped in `[]`?
	 * @param objects        The values to convert to Strings to put in the list
	 * @return               Comma-separated list of the values
	 */
	public static String toString( boolean wrapInBrackets, Object... objects )
	{
		String converted = Arrays.toString( objects );
		if( wrapInBrackets == false )
		{
			converted = converted.substring( 1 );
			converted = converted.substring( 0, converted.length()-1 );
		}
		
		return converted;
	}
	
	public static String getNormalizedFilePath( File file )
	{
		String absPath = file.getAbsolutePath();
		String normPath = absPath.replace( '\\', '/' );
		return normPath;
	}
	
	public static String getResourceString( String name )
	{
		InputStream in = ClassLoader.getSystemResourceAsStream( name );
		InputStreamReader streamReader = new InputStreamReader( in );
		BufferedReader bufferedReader = new BufferedReader( streamReader );
		return bufferedReader.lines().collect( Collectors.joining("\n") );
	}
}
