/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.job;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import com.calytrix.mips.Messages;
import com.calytrix.mips.MipsException;
import com.calytrix.mips.task.ITask;

/**
 * A job represents a collection of tasks that are executed in sequence to generate some form of output.
 * <p/>
 * To add a task to a job, call {@link #addTask(String, ITask, JSONObject)}. 
 * <p/>
 * To schedule a job for execution, call {@link ExecutionManager#submitJob(Job)}.
 */
public class Job
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------
	public static final String PROP_JOBNAME = "jobname";
	public static final String PROP_WORKINGDIR = "workingdir";
	public static final String PROP_POSTCLEANUP = "postcleanup";

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private List<TaskWrapper> tasks;
	private Set<IProgressListener> progressListener;
	private Properties properties;
	private volatile boolean running;
	
	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	/**
	 * Constructs a new Job instance with specified name and base properties.
	 * <p/>
	 * The properties for the job can be referenced by Tasks added to this job by calling 
	 * {@link #getProperty(String)}.
	 * 
	 * @param name the display name for the job
	 * @param properties user specified properties that can be referenced by Tasks added to this job
	 */
	public Job( String name, Properties properties )
	{
		this.running = false;
		this.progressListener = new CopyOnWriteArraySet<>();
		this.tasks = new ArrayList<TaskWrapper>();
		this.properties = new Properties();
		
		this.properties.put( PROP_WORKINGDIR, "${" + Job.PROP_JOBNAME + "}" );
		for( String key : properties.stringPropertyNames() )
			this.properties.put( key, properties.get(key) );
		this.properties.put( PROP_JOBNAME, name );
	}

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	/**
	 * Registers an {@link IProgressListener} that will be notified via callbacks whenever important
	 * events occur during this Job's execution.
	 * 
	 * @param listener the listener to receive progress callbacks
	 */
	public void addProgressListener( IProgressListener listener )
	{
		this.progressListener.add( listener );
	}
	
	/**
	 * Unregisters an {@link IProgressListener} so that it will no longer be notified whenever important
	 * events occur during this Job's execution.
	 * 
	 * @param listener the listener to receive progress callbacks
	 */
	public void removeProgressListener( IProgressListener listener )
	{
		this.progressListener.remove( listener );
	}
	
	/**
	 * Adds the specified task to the end of this job's task list.
	 * <p/>
	 * <b>Note:</b> This method can only be called while the job is not currently executing.
	 * 
	 * @param name the display name to give the task 
	 * @param impl the {@link ITask} instance to add
	 * @param parameters configuration parameters to pass to the task
	 * 
	 * @throws MipsException is this method is called while the job is currently executing
	 */
	public synchronized void addTask( String name, ITask impl, JSONObject parameters )
	{
		// Job must not be running
		if( this.running )
			throw new MipsException( Messages.get("Job.error.currentlyrunning") );
		
		TaskWrapper wrapper = new TaskWrapper( name, impl, parameters );
		this.tasks.add( wrapper );
	}
	
	/**
	 * Executes the tasks contained within this job.
	 * <p/>
	 * <b>Note:</b> This method has been deliberately marked private so that only the 
	 * {@link ExecutionManager} can access it.
	 * 
	 * @param logger the logger that system messages should be logged to
	 * @return a {@link JobResult} instance indicating whether the job succeeded or not.
	 */
	protected JobResult run( Logger logger )
	{
		synchronized( this )
		{
			// Make sure the job is not already running
			if( this.running )
				throw new MipsException(Messages.get("Job.error.alreadyrunning") );
			this.running = true;
		}
		
		String name = this.getJobName();
		Logger jobLogger = LogManager.getFormatterLogger( logger.getName() + ".job." + name );
		try
		{
			// Run pre-execution tasks for the job
			this.preExecute( jobLogger );
		}
		catch( Exception e )
		{
			return new JobResult( this, -1, e );
		}
		
		JobResult result = null;
		
		// Execute tasks in sequence
		for( int i = 0 ; i < this.tasks.size() ; ++i )
		{
			TaskWrapper wrapper = this.tasks.get( i );
			JSONObject parameters = wrapper.getParameters();
			ITask taskImpl = wrapper.getTask();	
			
			// Notify listeners that the task is starting
			this.notifyTaskStarted( jobLogger, i );
			
			try
			{
				// Execute the task
				taskImpl.execute( parameters, jobLogger );
			}
			catch( Exception e )
			{
				// If this task failed, then notify listeners and cancel further execution
				this.notifyTaskError( jobLogger, i, e );
				result = JobResult.createFailure( this, i, e );
				break;
			}
			
			// Notify listeners that the task completed
			this.notifyTaskComplete( jobLogger, i );
		}
		
		// If all tasks completed without error then mark the job as a success!
		if( result == null )
			result = JobResult.createSuccess( this );
		
		// Run post execution tasks
		this.postExecute( result, jobLogger );
		
		synchronized( this )
		{
			this.running = false;
		}
		
		return result;
	}
	
	/**
	 * Runs setup steps for this job before it executes. These steps include:
	 * <ol>
	 *  <li>Creating the working directory for the job</li>
	 *  <li>Configuring all tasks within the Job's task list</li>
	 *  <li>Notifying all progress listeners that the job is starting</li>
	 * </ol>
	 * @param logger the logger to log system messages to
	 * @throws MipsException if the working directory could not be created, or an exception occurred while
	 *                       trying to configure a task
	 */
	private void preExecute( Logger logger ) throws MipsException
	{
		File workingDirectory = this.getWorkingDirectory();
		if( !workingDirectory.exists() )
		{
			logger.debug( Messages.get("Job.creatingworkingdir"), 
			              workingDirectory.getAbsolutePath() );
			boolean dirsMade = workingDirectory.mkdirs();
			if( !dirsMade )
			{
				String message = String.format( Messages.get("Job.error.createworkingdir"),
				                                workingDirectory.getAbsolutePath() );
				logger.error( message );
				throw new MipsException( message );
			}
		}
		
		// Configure all tasks
		for( int i = 0 ; i < this.tasks.size() ; ++i )
		{
			TaskWrapper wrapper = this.tasks.get( i );
			ITask impl = wrapper.getTask();
			try
			{
				impl.configure( this );
			}
			catch( Exception e )
			{
				throw new MipsException( e );
			}
		}
		
		logger.info( Messages.get("Job.starting") );
		for( IProgressListener listener : this.progressListener )
			listener.jobStarted( this );
	}
	
	/**
	 * Runs teardown steps for this job after it has finished executing. These steps include:
	 * <ol>
	 *  <li>Cleaning up the working directory</li>
	 *  <li>Notifying all progress listeners that the job has completed</li>
	 * </ol>
	 * @param result the result of the job's execution
	 * @param logger the logger to log system messages to
	 */
	private void postExecute( JobResult result, Logger logger )
	{
		if( isPostCleanup() )
		{
			File workingDirectory = this.getWorkingDirectory();
			logger.debug( Messages.get("Job.cleaningworkingdir"),
			              workingDirectory.getAbsolutePath() );
			
			// Clean up working directory
			Path workingPath = workingDirectory.toPath();
			
			try
			{
				Files.walk( workingPath )
				     .sorted( Comparator.reverseOrder() )
				     .map( Path::toFile )
				     .forEach( File::delete );
			}
			catch( IOException ioe )
			{
				logger.warn( Messages.get("Job.error.cleanworkingdir"),
				             workingDirectory.getAbsolutePath(),
				             ioe.getLocalizedMessage() );
			}
		}
		
		for( IProgressListener listener : this.progressListener )
			listener.jobComplete( result );
		logger.info( Messages.get("Job.complete") );
	}
	
	private void notifyTaskStarted( Logger logger, int taskIndex )
	{
		TaskWrapper wrapper = this.tasks.get( taskIndex );
		logger.debug( Messages.get("Job.taskstarting"),
		              wrapper.getName(),
		              taskIndex + 1,
		              this.tasks.size() );
		
		for( IProgressListener listener : this.progressListener )
			listener.taskStarted( this, taskIndex );
	}
	
	private void notifyTaskError( Logger logger,
	                              int taskIndex,
	                              Throwable cause )
	{
		TaskWrapper wrapper = this.tasks.get( taskIndex );
		String message = String.format( Messages.get("Job.taskfailed"), 
		                                wrapper.name,
		                                taskIndex + 1,
		                                this.tasks.size(),
		                                cause.getLocalizedMessage() );
		logger.error( message, cause );
		
		for( IProgressListener listener : this.progressListener )
			listener.taskError( this, taskIndex, cause );
	}
	
	private void notifyTaskComplete( Logger logger,
	                                 int taskIndex )
	{
		TaskWrapper wrapper = this.tasks.get( taskIndex );
		logger.debug( Messages.get("Job.taskcomplete"),
		              wrapper.getName(),
		              taskIndex + 1,
		              this.tasks.size() );
		
		for( IProgressListener listener : this.progressListener )
			listener.taskComplete( this, taskIndex );
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Accessor and Mutator Methods ///////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	public String getJobName()
	{
		return this.getProperty( PROP_JOBNAME );
	}

	public File getWorkingDirectory()
	{
		File workingDir = new File( System.getProperty("user.home"), this.getJobName() );
		if( this.properties.containsKey(PROP_WORKINGDIR) )
		{
			String path = this.getProperty( PROP_WORKINGDIR );
			workingDir = new File( path );
		}
		return workingDir;
	}
	
	public String getTaskName( int index )
	{
		TaskWrapper wrapper = this.tasks.get( index );
		return wrapper.getName();
	}
	
	public int getTaskCount()
	{
		return this.tasks.size();
	}	
	
	private boolean isPostCleanup()
	{
		boolean postCleanup = true;
		if( this.properties.containsKey(PROP_POSTCLEANUP) )
			postCleanup = this.getProperty( PROP_POSTCLEANUP ).equals( Boolean.TRUE );
		
		return postCleanup;
	}
	
	/**
	 * Substitutes any property tokens in <code>value</code> for their corresponding job property values.
	 * <p/>
	 * For example, if the job contained the property <code>"somevalue": "foo"</code> then a 
	 * <code>value</code> parameter of <code>${somevalue}.txt</code> would be transformed to 
	 * <code>foo.txt</code>
	 * 
	 * @param value the value to perform property token substitution on
	 * @return the <code>value</code> parameter with all property tokens substituted for their 
	 *         corresponding values 
	 */
	public String substitutePropertyTokens( String value )
	{
		int iterationCount = 0;
		while( value.contains("${") && iterationCount++ < 16 )
		{
			for( String key : this.properties.stringPropertyNames() )
				value = value.replace( "${" + key + "}", this.properties.getProperty(key) );
		}
		return value;
	}
	
	public File getPathRelativeTo( String path, File relativeTo )
	{
		String substitutedPath = substitutePropertyTokens( path );
		File asFile = new File( substitutedPath );
		if( !asFile.isAbsolute() )
			asFile = new File( relativeTo, substitutedPath );
		
		return asFile;
	}
	
	/**
	 * Returns the value of the job property keyed against the specified name
	 * 
	 * @param name the name of the property to return the value for
	 * @return the value of the specified property
	 */
	public String getProperty( String name )
	{
		String valueRaw = this.properties.getProperty( name );
		return substitutePropertyTokens( valueRaw );
	}
	
	/**
	 * Sets the value of the job property wiht the specified name
	 * 
	 * @param name the name of the property to set
	 * @param value the value of the property to set
	 */
	public void setProperty( String name, String value )
	{
		this.properties.setProperty( name, value );
	}
	
	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
	private class TaskWrapper
	{
		private String name;
		private ITask task;
		private JSONObject params;
		
		public TaskWrapper( String name, ITask task, JSONObject params )
		{
			this.name = name;
			this.task = task;
			this.params = params;
		}
		
		public String getName()
		{
			return this.name;
		}
		
		public ITask getTask()
		{
			return this.task;
		}
		
		public JSONObject getParameters()
		{
			return this.params;
		}
	}
}
