/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.job;

/**
 * Represents the result of a Job's execution.
 * <p/>
 * To determine whether the Job's execution was successful, call {@link #isSuccess()}.
 * <p/>
 * If the job was not successful, then the index of the task that failed, and the reason for failure can
 * be obtained by calling {@link #getFailedTaskIndex()} and {@link #getFailureCause()} respectively.
 */
public class JobResult
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private Job job;
	private int failedTaskIndex;
	private Throwable failureCause;

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	public JobResult( Job job )
	{
		this( job, -1, null );
	}

	public JobResult( Job job, int failedTaskIndex, Throwable cause )
	{
		this.job = job;
		this.failedTaskIndex = failedTaskIndex;
		this.failureCause = cause;
	}
	
	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------

	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Accessor and Mutator Methods ///////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * @return the {@link Job} that this result was generated for
	 */
	public Job getJob()
	{
		return this.job;
	}
	
	/**
	 * @return <code>true</code> if this job executed successfully to completion, or <code>false</code>
	 *         if an error occurred during the job's execution
	 */
	public boolean isSuccess()
	{
		return this.failedTaskIndex < 0;
	}

	/**
	 * @return the index of the task that failed, or <code>-1</code> if the job completed successfully
	 * 
	 * @see #isSuccess()
	 * @see #getFailureCause()
	 */
	public int getFailedTaskIndex()
	{
		return this.failedTaskIndex;
	}
	
	/**
	 * @return the error that was thrown during the job's execution, or <code>null</code> if the job 
	 *         completed successfully
	 * 
	 * @see #isSuccess()
	 * @see #getFailedTaskIndex()
	 */
	public Throwable getFailureCause()
	{
		return this.failureCause;
	}
	
	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
	public static JobResult createSuccess( Job job )
	{
		return new JobResult( job );
	}
	
	public static JobResult createFailure( Job job, int taskIndex, Throwable cause )
	{
		return new JobResult( job, taskIndex, cause );
	}
}
