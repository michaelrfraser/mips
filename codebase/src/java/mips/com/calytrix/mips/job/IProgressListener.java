/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.job;

/**
 * An interface through which a {@link Job} can notify external components of its execution progress. 
 */
public interface IProgressListener
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	/**
	 * Indicates that the specified {@link Job} has begun execution
	 * 
	 * @param job the {@link Job} that has begun execution
	 */
	public void jobStarted( Job job );
	
	/**
	 * Indicates that the {@link ITask} at the specified index of <code>job</code> has begun execution
	 * 
	 * @param job the {@link Job} that the task is a part of
	 * @param index the index of the {@link ITask} within the job
	 * 
	 * @see Job#getTaskName(int)
	 */
	public void taskStarted( Job job, int index );
	
	/**
	 * Indicates that the {@link ITask} at the specified index of <code>job</code> has reported progress
	 * within its execution.
	 * 
	 * @param job the {@link Job} that the task is a part of
	 * @param index the index of the {@link ITask} within the job
	 * @param progress a value between 0..1 indicating how far the task is through its execution 
	 * 
	 * @see Job#getTaskName(int)
	 */
	public void taskProgress( Job job, int index, double progress );
	
	/**
	 * Indicates that the {@link ITask} at the specified index of <code>job</code> has completed execution
	 * 
	 * @param job the {@link Job} that the task is a part of
	 * @param index the index of the {@link ITask} within the job
	 * 
	 * @see Job#getTaskName(int)
	 */
	public void taskComplete( Job job, int index );
	
	/**
	 * Indicates that the {@link ITask} at the specified index of <code>job</code> encountered an error
	 * during its execution.
	 * <p/>
	 * <b>Note</b> Subsequent tasks belonging to the {@link Job} will not be executed if an error is
	 * encountered
	 * 
	 * @param job the {@link Job} that the task is a part of
	 * @param index the index of the {@link ITask} within the job
	 * @param error the error that was encountered
	 * 
	 * @see Job#getTaskName(int)
	 */
	public void taskError( Job job, int index, Throwable error );
	
	/**
	 * Indicates that a {@link Job} has completed execution.
	 * <p/>
	 * The {@link Job} that completed execution can be obtained by calling {@link JobResult#getJob()}.
	 * <p/>
	 * To determine whether the {@link Job} completed successfully, call {@link JobResult#isSuccess()}.
	 * 
	 * @param result a {@link JobResult} instance that specifies the job that completed, and whether it 
	 *               completed successfully.
	 */
	public void jobComplete( JobResult result );
	
	/**
	 * Indicates that execution of the specified job was cancelled
	 * 
	 * @param job the {@link Job} that was cancelled
	 */
	public void jobCancelled( Job job );
}
