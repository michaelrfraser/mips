/*
 *   Copyright 2018 Calytrix Technologies
 *
 *   This file is part of the Calytrix Master Import Pipeline Service.
 *
 *   NOTICE:  All information contained herein is, and remains
 *            the property of Calytrix Technologies Pty Ltd.
 *            The intellectual and technical concepts contained
 *            herein are proprietary to Calytrix Technologies Pty Ltd.
 *            Dissemination of this information or reproduction of
 *            this material is strictly forbidden unless prior written
 *            permission is obtained from Calytrix Technologies Pty Ltd.
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 */
package com.calytrix.mips.job;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.calytrix.mips.Messages;
import com.calytrix.mips.MipsException;
import com.calytrix.mips.configuration.Configuration;
import com.calytrix.mips.configuration.ConfigurationException;

/**
 * The Execution Manager is responsible for the execution of jobs within the MIPS framework.
 * <p/>
 * Like other MIPS components, the Execution Manager must first be started via a call to 
 * {@link #startup()} before it 
 * To queue a job for execution, call {@link #submitJob(Job)}.
 */
public class ExecutionManager
{
	//----------------------------------------------------------
	//                    STATIC VARIABLES
	//----------------------------------------------------------

	//----------------------------------------------------------
	//                   INSTANCE VARIABLES
	//----------------------------------------------------------
	private IProgressListener forwarder;
	private Set<IProgressListener> listeners;
	private ExecutorService executor;
	private int executorThreads;
	private Logger logger;

	//----------------------------------------------------------
	//                      CONSTRUCTORS
	//----------------------------------------------------------
	public ExecutionManager( Logger logger )
	{
		this.listeners = new CopyOnWriteArraySet<IProgressListener>();
		this.forwarder = new ForwarderListener();
		this.executorThreads = Runtime.getRuntime().availableProcessors();
		this.logger = LogManager.getFormatterLogger( logger.getName() + ".execman" );
	}

	//----------------------------------------------------------
	//                    INSTANCE METHODS
	//----------------------------------------------------------
	public void configure( Configuration configuration ) throws ConfigurationException
	{
		
	}
	
	/**
	 * Starts the {@link ExecutionManager}, allowing jobs to be submitted via the {@link #submitJob(Job)}
	 * method.
	 */
	public void startup()
	{
		this.executor = Executors.newFixedThreadPool( this.executorThreads );
	}
	
	/**
	 * Stops the {@link ExecutionManager}, waiting until all jobs have been completed, or cancelled
	 * before returning.
	 * 
	 */
	public void shutdown()
	{
		this.executor.shutdown();
		
		try
		{
			this.executor.awaitTermination( 5000, TimeUnit.MILLISECONDS );
		}
		catch( InterruptedException ie )
		{
			logger.warn( Messages.get("ExecutionManager.error.interruptedshutdown"), ie );
		}
		this.executor = null;
	}
	
	/**
	 * Submits the specified job for execution in the future.
	 * 
	 * @param job the {@link Job} to execute.
	 * @return a {link Future} through which the job's result can be accessed once its execution is 
	 *         complete
	 * @throws IllegalStateException if the execution manager has not been started
	 */
	public Future<JobResult> submitJob( Job job )
	{
		if( this.executor == null )
			throw new IllegalStateException();
		
		job.addProgressListener( this.forwarder );
		return this.executor.submit( new Callable<JobResult>(){
			@Override
			public JobResult call() throws Exception
			{
				ExecutionManager manager = ExecutionManager.this;
				JobResult result = job.run( manager.getLogger() );
				job.removeProgressListener( forwarder );
				return result;
			}
		});
	}
	
	/**
	 * Registers an {@link IProgressListener} that will be notified via callbacks whenever Jobs submitted
	 * to this execution manager reach milestones during their execution. 
	 * 
	 * @param listener the listener to receive progress callbacks
	 */
	public void addProgressListener( IProgressListener listener )
	{
		this.listeners.add( listener );
	}
	
	/**
	 * Unregisters an {@link IProgressListener} so that no longer will be notified via callbacks whenever 
	 * Jobs submitted to this execution manager reach milestones during their execution. 
	 * 
	 * @param listener the listener to receive progress callbacks
	 */
	public void removeProgressListener( IProgressListener listener )
	{
		this.listeners.remove( listener );
	}
	
	////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////// Accessor and Mutator Methods ///////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////
	public int getThreadCount()
	{
		return this.executorThreads;
	}
	
	public Logger getLogger()
	{
		return this.logger;
	}
	
	//----------------------------------------------------------
	//                     STATIC METHODS
	//----------------------------------------------------------
	/**
	 * Convenience method that will start the specified executable in a new process.
	 * <p/>
	 * The created process' standard out and err will be redirected to 
	 * <code>${workingDirectory}/${executable}.out</code> and
	 * <code>${workingDirectory}/${executable}.err</code> respectively.
	 * 
	 * @param executable the file to execute 
	 * @param args command line arguments to pass to the executable
	 * @param workingDirectory the working directory that the process should be started in
	 * @return a {@link Process} instance representing the newly created process that the executable is
	 *         running within
	 * 
	 * @throws IOException if there was an error starting the executable
	 * @throws IllegalArgumentException if the working directory could not be created
	 */
	public static Process spawnProcess( File executable, List<String> args, File workingDirectory )
			throws IOException, IllegalArgumentException
	{
		if( !workingDirectory.exists() )
		{
			boolean workingDirMade = workingDirectory.mkdirs();
			if( !workingDirMade )
			{
				String message = String.format( Messages.get("ExcutionManager.error.createworkingdir"), 
				                                workingDirectory.getAbsoluteFile() );
				throw new IllegalArgumentException( message );
			}
		}
		
		File stdout = new File( workingDirectory, executable.getName() + ".out" );
		File stderr = new File( workingDirectory, executable.getName() + ".err" );
		
		String[] command = new String[args.size() + 1];
		command[0] = executable.getAbsolutePath();
		for( int i = 0 ; i < args.size() ; ++i )
			command[1+i] = args.get( i );
		
		ProcessBuilder builder = new ProcessBuilder( command );
		builder.directory( workingDirectory );
		builder.redirectOutput( stdout );
		builder.redirectError( stderr );
		return builder.start();
	}
	
	/**
	 * Waits for the provided process to terminate.
	 * <p/>
	 * If the process returns a non-zero exit code, then a {@link MipsException} is thrown.
	 * 
	 * @param process the process to wait for
	 * @param executable the executable that is running within the process
	 * @throws MipsException if the process returns a non-zero exit code
	 * @throws InterruptedException if the execution manager was interrupted before the process finished
	 *                              executing 
	 */
	public static void waitForProcess( Process process, File executable )
		throws MipsException, InterruptedException
	{
		int exitCode = process.waitFor();
		if( exitCode != 0 )
		{
			String message = String.format( Messages.get("ExecutionManager.error.execcode"), 
			                                executable.getAbsolutePath(),
			                                exitCode );
			throw new MipsException( message );
		}
	}
	
	/**
	 * Private {@link IProgressListener} that forwards progress callbacks from {@link Job} instances
	 * to listeners registered to this {@link ExecutionManager} instance. 
	 */
	private class ForwarderListener implements IProgressListener
	{
		@Override
		public void jobStarted( Job job )
		{
			for( IProgressListener listener : listeners )
				listener.jobStarted( job );
		}

		@Override
		public void taskStarted( Job job, int index )
		{
			for( IProgressListener listener : listeners )
				listener.taskStarted( job, index );
		}

		@Override
		public void taskProgress( Job job, int index, double progress )
		{
			for( IProgressListener listener : listeners )
				listener.taskProgress( job, index, progress );
		}
		
		@Override
		public void taskComplete( Job job, int index )
		{
			for( IProgressListener listener : listeners )
				listener.taskComplete( job, index );
		}

		@Override
		public void taskError( Job job, int index, Throwable error )
		{
			for( IProgressListener listener : listeners )
				listener.taskError( job, index, error );
		}

		@Override
		public void jobComplete( JobResult result )
		{
			for( IProgressListener listener : listeners )
				listener.jobComplete( result );
		}

		@Override
		public void jobCancelled( Job job )
		{
			for( IProgressListener listener : listeners )
				listener.jobCancelled( job );
		}
	}
}
