#!/bin/bash

#################################
# Set up the Installer Defaults #
#################################
# build variables -- can be modified by command line arguments
# NOTE: The version and install path tokens will be replaced by Ant
VERSION=__VERSION__
USAGE="Usage: installer.sh [-nh] [-i INSTALL_PATH] [-l LAUNCHER_PATH]"
INTERACTIVE=1                    # interactive by default
INSTALL_PATH="__INSTALL_PATH__"
LAUNCHER_PATH="/usr/bin"         # install into /usr/bin by default

####################################
# Parse the command line arguments #
####################################
while getopts hni:l: OPT; do
    case "$OPT" in
        h)
            echo
            echo $USAGE
            echo
            echo "  -h    Display this help message"
            echo "  -n    Non-interactive mode (user is not prompted for input)"
            echo "  -i    Path to install One World Server (default: $INSTALL_PATH)"
            echo "  -l    Path to install system-wide launchers into (default: $LAUNCHER_PATH)"
            echo
            echo "NOTE: You must be root to install One World Server or launchers into system-wide"
            echo "      areas such as /usr/bin"
            exit 0
            ;;
        n)
            INTERACTIVE=0
            ;;
        i)
            INSTALL_PATH=$OPTARG
            ;;
        l)
            LAUNCHER_PATH=$OPTARG
            ;;
        \?)
            echo $USAGE >&2
            echo "Run $0 -h for further details on usage"
            exit -1
            ;;
    esac
done

#################################################################################################
# Let's run this install!                                                                       #
# Print out a welcome header, get license agreement, figure out where to dump stuff and DO EET! #
#################################################################################################
main()
{
    welcome

    if [ $INTERACTIVE -eq 1 ]; then
        eula
        getInstallPaths
    fi

	install
}

####################################################################################
#                           Core Installation Functions                            #
####################################################################################
install()
{
	echo Installing One World Server...
	echo Install path: $INSTALL_PATH
	echo

	# move the contents of the working directory over to the install directory
	if [ ! -d "$INSTALL_PATH" ]; then
  		echo "[INSTALL] Creating target installation directory"
  		mkdir -p $INSTALL_PATH
	fi

  # update the sample service script with the nominated installation path
  echo "[INSTALL] Updating sample service script with specified target"
  ESCAPED_INSTALL_PATH=$(echo $INSTALL_PATH | sed 's/[\/&]/\\&/g')
  sed -i -e "s/__APPDIR__/$ESCAPED_INSTALL_PATH/" ows

	# copy the contents over to the install directory
	echo "[INSTALL] Copying files over to target"
	mv * $INSTALL_PATH/

	# remove the unnecessary files in the install directory
  echo "[INSTALL] Removing temporary installation files"
	rm $INSTALL_PATH/installer.sh   # this installation script

  echo "[INSTALL] Completed installation to [$INSTALL_PATH]"
}

####################################################################################
#                                  Welcome Header                                  #
####################################################################################
welcome()
{
	# print a welcome message
	echo
	echo "=================================================================="
	echo "Calytrix One World Server v$VERSION Installer"
	echo "Copyright (c) Calytrix Technologies $(date +%Y)"
	echo "=================================================================="
	echo
}

####################################################################################
#                       End User License Agreement Functions                       #
####################################################################################
eula()
{
	echo "By installing this software you must agree the terms of the End User License Agreement"
    yesno "Do you wish to review the EULA?" "y"
    if [ "$YNRESULT" == "y" ]; then
        cat LICENSE | less
    fi

    echo "Do you accept the license agreement?"
    yesno "Do you wish to accept the license agreement?" "y"
    if [ "$YNRESULT" == "n" ]; then
    	echo "Aborting installation because license was not accepted"
        exit 0;
    fi
}

####################################################################################
#                           Installation Path Functions                            #
####################################################################################
getInstallPaths()
{
	# figure out where they want to install
    readstring "Enter directory to install in:" $INSTALL_PATH
    INSTALL_PATH=$READRESULT
    echo ""
}

####################################################################################
#                             General Helper Functions                             #
####################################################################################
#
# Params
# - $1 The prompt to display to the user
# - $2 The default value
#
# Return
# - Sets the variable $READRESULT to the entered value
#
readstring()
{
    echo -e "$1"
    printf "[$2] "
    read READRESULT

    if [ "$READRESULT" == "" ]; then
        READRESULT=$2
    fi
}

#
# Prompts the user for yes/no input
#
# Params
# - $1 The prompt to display to the user
# - $2 The default option (y or n)
#
# Return
# - Sets the variable $YNRESULT to either "y" or "n"
#
yesno()
{
    local VALIDINPUT=0
    local PROMPT=
    local RESULT=
    if [ "$2" == "y" ]; then
        PROMPT="[Yn] "
    else
        PROMPT="[yN] "
    fi

    while [ $VALIDINPUT -eq 0 ]; do
        echo -e "$1"
        printf $PROMPT
        read RESULT

        if [ "$RESULT" == "y" ] || [ "$RESULT" == "Y" ]; then
            YNRESULT=y
            VALIDINPUT=1
        elif [ "$RESULT" == "n" ] || [ "$RESULT" == "N" ]; then
            YNRESULT=n
            VALIDINPUT=1
        elif [ "$RESULT" == "" ]; then
            YNRESULT=$2
            VALIDINPUT=1
        else
            error "Unexpected input: please enter either 'y' or 'n'"
            echo
        fi
    done
    printf "\n"
}

### Run things ###
main
exit 0
