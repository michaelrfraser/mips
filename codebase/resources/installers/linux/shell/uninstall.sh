#!/bin/bash

# NOTE: The version token will be replaced by Ant
VERSION=__VERSION__

# $1 - String with a question for the user.
function ask() {
  while true; do
    read -p "$1 " yn
    case $yn in
      y|Y)
        local answer=1
        break
      ;;
      n|N)
        local answer=0
        break
      ;;
      # TODO: Shell never seems to print the default case when it happens
      *)
        echo "Unrecognised option"
      ;;
    esac
  done
  echo $answer
}

echo
echo "=================================================================="
echo "Calytrix One World Server v$VERSION Uninstaller"
echo "Copyright (c) Calytrix Technologies $(date +%Y)"
echo "=================================================================="
echo

REMOVE_LOGS=$(ask "Keep log files? (y/n)")
UNINSTALL=$(ask "Proceed with uninstall? (y/n)")

if $UNINSTALL == 1; then
  # TODO
fi
