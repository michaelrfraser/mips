#!/bin/bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MIPS_HOME=$DIR/..
PATH=$MIPS_HOME/jre/bin:$PATH
java -Xss8M -jar $MIPS_HOME/lib/mips.jar $*

